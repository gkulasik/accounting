Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false
  
  # Dev Mailer config, default url option (devise)
  config.action_mailer.default_url_options = { host: ENV['HOST']} # Edited for Cloud9
  
  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true
  
  # Use this to white list IPs for the webconsole interface (only works with local host otherwise)
  # Never worked with Cloud9 - moved to development.rb due to heroku error in application.rb
  config.web_console.whitelisted_ips =  %w( 127.0.0.1 0.0.0.0)

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  
  # Development Sparkpost set up - Dev Email
  config.action_mailer.delivery_method = :sparkpost
  
  # Sets up Bullet for testing for bad queries
  config.after_initialize do
  Bullet.enable = true
  Bullet.alert = true
  Bullet.bullet_logger = true
  Bullet.console = true
  Bullet.rails_logger = true
  BULLET_DEBUG=true
  end
 
# Paperclip development settings 
  config.paperclip_defaults = {
  :storage => :s3,
  :s3_credentials => {
    :bucket => ENV['S3_BUCKET'],
    :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
    :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
  }
}

end
# Better errors
BetterErrors::Middleware.allow_ip! ENV['TRUSTED_IP'] if ENV['TRUSTED_IP']
Paperclip.options[:command_path] = "/usr/bin/"
# TRUSTED_IP=[your ip, no quotes] rails s -b 0.0.0.0    // for better errors to get it to stop as intended


