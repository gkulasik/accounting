# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Sanitize set up - available as universal method due to placement.
def html_sanitizer(text, array=[])
    # Create a new scrubber - will target specific words
    scrubber = Rails::Html::TargetScrubber.new
    # make sure 'script' is one of the words it will go after
    array << "script"
    scrubber.tags = array
    # Put the text in a loofah for scrubbing
    html_fragment = Loofah.fragment(text)
    html_fragment.scrub!(scrubber) # scrub - remove the offending words
    html_fragment.to_s # return clean string
end


# Initialize the Rails application.
Rails.application.initialize!
