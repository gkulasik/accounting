# USE THIS FILE TO SET SYSTEM DEFAULTS ON RELEASE/LAUNCH 
# Starts with '0' to guarantee it starts before regular initializers - since some initializers may rely on items set in this file.
# Can customize by env
require 'boolean_env_wrapper'
BOOL_ENV = BooleanEnvWrapper.new(ENV) # Sets Boolean environment processor ("false" == true cause its a string)
# usage: BOOL_ENV.fetch('ENV_VAR_NAME')
