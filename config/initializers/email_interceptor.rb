require 'development_email_interceptor'
require 'staging_email_interceptor'
ActionMailer::Base.register_interceptor(DevelopmentEmailInterceptor) if Rails.env.development?
ActionMailer::Base.register_interceptor(StagingEmailInterceptor) if BOOL_ENV.fetch('IS_STAGING')