# Acts as Taggable on Config 

# Force lower case to avoid duplication based on upper/lower case
ActsAsTaggableOn.force_lowercase = true