class Ahoy::Store < Ahoy::Stores::ActiveRecordStore
  def track_visit(options)
    if BOOL_ENV.fetch('TRACK_ACTIVITY')
        super
    end
  end

  def track_event(name, properties, options)
    if BOOL_ENV.fetch('TRACK_ACTIVITY')
      id = properties[:id] # when the property is set as id: xx then need to do properties[:id]
      properties.delete(:id) # don't want it adding into the properties as well
        super do |event|
            event.item_id = id 
        end
    end
  end
end
