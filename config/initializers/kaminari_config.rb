# Kaminari configuration

# Test - make the per page default smaller to have less data for tests. 
if Rails.env.test?
  Kaminari.configure do |config|
    config.default_per_page = 8
    # config.max_per_page = nil
    # config.window = 4
    # config.outer_window = 0
    # config.left = 0
    # config.right = 0
    # config.page_method_name = :page
    # config.param_name = :page
  end
else
  # Regular Kaminari config - Dev and Prod (and staging)
  Kaminari.configure do |config|
    # config.default_per_page = 25
    # config.max_per_page = nil
    # config.window = 4
    # config.outer_window = 0
    # config.left = 0
    # config.right = 0
    # config.page_method_name = :page
    # config.param_name = :page
  end
end