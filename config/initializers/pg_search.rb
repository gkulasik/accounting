# PGSearch config - add extra search options to make search 'better' and more flexible 
PgSearch.multisearch_options = {
  :using => {
    :tsearch => {
      :prefix => true,
      :dictionary => "english"
    },
    :trigram => {
      :threshold => 0.1
      }
  },
  :ignoring => :accents
}