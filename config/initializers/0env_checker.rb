# FILE PURPOSE: TO CHECK ALL ENV VARIABLES ARE SET CORRECTLY - AVOIDS HEADACHES
# This file should be updated each time a new ENV variable is added to application.yml
# Will fail on launch and throw exception (so we know what the issue is)
ENV.fetch('AWS_ACCESS_KEY_ID')
ENV.fetch('AWS_SECRET_ACCESS_KEY')
ENV.fetch('DEFAULT_ROBOT_EMAIL')
ENV.fetch('SPARKPOST_API_KEY')
ENV.fetch('WEB_CONCURRENCY')
ENV.fetch('REDIRECT_EMAIL')
ENV.fetch('IS_STAGING')
ENV.fetch('S3_BUCKET') unless !Rails.env.testing? # test env does not send any data to buckets - so there is no bucket
ENV.fetch('TRACK_ACTIVITY')
ENV.fetch('HOST')
