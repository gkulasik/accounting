require 'test_helper'

class RegistrationsControllerTest < ActionController::TestCase
    
  setup do
    # Set up controller for rails and devise mapping for helpers
    @controller = Users::RegistrationsController.new
    request.env['devise.mapping'] = Devise.mappings[:user]
  end
  
  teardown do
    # Use this to remove the user avatar folders that are created when tests are run as Paperclip does not transmit to S3 during tests
    if File.exists?("#{Rails.root}/pierce777")
      FileUtils.remove_dir "#{Rails.root}/pierce777", false
    end
  end
  
  test "should update profile with valid image" do
    user = users(:correct)
    sign_in user
    # create file to upload
    file = fixture_file_upload("bunny.png", "image/png")
    # update
    patch :update, id: user, user: {first_name: "k", last_name: "kulasik", current_password: "password123", avatar: file}
    user = user.reload
    # asssert image exists and update took place
    assert_equal("bunny.png", user.avatar_file_name)
    assert_equal("k", user.first_name)
    assert_redirected_to user_path(user.username)
  end
  test "should update profile with valid image 2" do
    user = users(:correct)
    sign_in user
    # create file
    file = fixture_file_upload("bunny.jpeg", "image/jpeg")
    # update
    patch :update, id: user, user: {first_name: "k", last_name: "kulasiksi", current_password: "password123", avatar: file}
    user = user.reload
    # assert image exists and update went through
    assert_equal("bunny.jpeg", user.avatar_file_name)
    assert_equal("k", user.first_name)
    assert_redirected_to user_path(user.username)
  end
  
  test "should update profile to delete image" do
    user = users(:correct)
    sign_in user
    # update - to delete image
    patch :update, id: user, user: {first_name: user.first_name, last_name: user.last_name, current_password: "password123", delete_image: "1"}
    user = user.reload
    # assert image is gone
    assert_nil(user.avatar_file_name)
    assert_redirected_to user_path(user.username)
  end
  
  test "should not update profile with invalid image" do
    user = users(:correct)
    sign_in user
    # test multiple file types that are invalid 
    file_array = ["test_csv.csv", "test_excel - Copy.xlsx", "test_excel.xls", "test_csv.csv", "test_csv.csv"]
    type_array = ["text/csv", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel", "image/jpeg", "image/png"]
     # for each invalid type try to save and assert fails
     5.times do |i|
      # create file
        file = fixture_file_upload(file_array[i], type_array[i])
        # update
        patch :update, id: user, user: {first_name: "gregk", last_name: "kulasik", current_password: "password123", avatar: file}
        user = user.reload
        # assert not updated
        assert_not_equal(file_array[i], user.avatar_file_name)
        assert_not_equal("gregk", user.first_name)
        assert_response :success # return to edit screen
    end
  end
  
  
  ##### View Tests #####
  
  
  test "user new display correct" do
    get :new
    # assert that sign up page looks a certain way
    assert_select 'title', I18n.t('page_titles.models.user.new')
    # Header is same as title
    assert_select 'h2', I18n.t('page_titles.models.user.new')
    # 1 form with 6 inputs (general info)
    assert_select "form", 1
    assert_select "form input", 6
    # login link
    assert_select "a", "Log in"
  end
  
  
  test "user edit display correct" do
    sign_in users(:correct)
    get :edit, {username: users(:correct).username}
    # Assert the edit profile page looks correct. 
    assert_select 'title', I18n.t('page_titles.models.user.edit')
    # header is same as title
    assert_select 'h2', I18n.t('page_titles.models.user.edit')
    # 1 form
    assert_select "form", 1
  end
end