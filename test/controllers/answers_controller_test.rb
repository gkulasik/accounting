require 'test_helper'

class AnswersControllerTest < ActionController::TestCase

  setup do
    # Set up of many answer tests
    # Default answer to work with
     @answer = answers(:one)
    # File for attachments
     file = fixture_file_upload("test_csv.csv", "text/csv")
     @spread_sheet_answer = answers(:one)
     @spread_sheet_answer.update(spread_sheet_upload_attributes: {sheet: file})
    # Answer with attachment to work with
     @spread_sheet_answer = @spread_sheet_answer.reload
     @ahoy = Ahoy::Tracker.new
  end

  # teardown do
  #   # Use this to remove the /answers folder that is created when tests are run as Paperclip does not transmit to S3 during tests
  #   if File.exists?("#{Rails.root}/answer")
  #   FileUtils.remove_dir "#{Rails.root}/answer", false
  #   end
  # end
  
  test "should create answer" do
    # Test that simply creates an answer
    sign_in users(:correct)
    assert_difference('Answer.count') do
      post :create, answer: { answer_text: "this is a test answer", question_id: users(:correct).questions.first}
    end
    assert_redirected_to question_path(users(:correct).questions.first)
  end

  test "should create answer with valid attachment type" do
    sign_in users(:correct)
    # Create array of various file types (all valid)
    file_array = ["test_csv.csv", "test_excel - Copy.xlsx", "test_excel.xls"]
    type_array = ["text/csv", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel"]
    0..3.times do |i|
      # Tests multiple file types in a loop to make sure that all of them pass this test.
      file = fixture_file_upload(file_array[i], type_array[i]) 
      before = SpreadSheetUpload.count
      # Assert that answer is created
      assert_difference('Answer.count') do
       post :create, answer: {answer_text: "this is a test answer", question_id: users(:correct).questions.first, spread_sheet_upload_attributes: {sheet: file}}
      # Assert no errors
       assert_empty assigns(:answer).errors.messages
      end
      after = SpreadSheetUpload.count
      # Check that the spreadsheet upload count increased
      assert_equal(before+1, after, "attached spread sheet NOT created when should be") # basically assert_difference("SpreadSheetUpload.count")
      file.close
      assert_redirected_to assigns(:answer).question # successfully return to the answers question page
    end
  end
  
  test "should not create answer with invalid attachment type" do
    sign_in users(:correct)
    # Create array of file types (all invalid)
    # Even tests one with modified extension
    file_array = ["bunny.jpeg", "notXLSX.txt", "test_WORD.docx", "notXLSX.xls"]
    type_array = ["image/jpeg", "text/text", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-excel"]
    0..3.times do |i|
       # Tests multiple file types in a loop to make sure that none of them pass this test.
      file = fixture_file_upload(file_array[i], type_array[i])
      before = SpreadSheetUpload.count
      # Assert not created 
      assert_no_difference('Answer.count') do
       post :create, answer: {answer_text: "this is a test answer", question_id: users(:correct).questions.first, spread_sheet_upload_attributes: {sheet: file}}
      # Assert some errors
       assert_not_empty assigns(:answer).errors.messages
      end
      # Assert no change in count
      after = SpreadSheetUpload.count
      assert_equal(before, after, "attached spread sheet NOT created when should be") # basically assert_difference("SpreadSheetUpload.count")
      file.close
      assert_response :success # successfully go to edit errors page (new)
    end
  end

  test "should get edit" do
    # Get edit page
    sign_in @answer.user
    get :edit, id: @answer
    assert_response :success
  end

  test "should update answer" do
    # Simply updates answer
    sign_in @answer.user
    patch :update, id: @answer, answer: { answer_text: "this is a second test answer", question_id: users(:correct).questions.first, user_id: @answer.user.id}
    assert_redirected_to question_path(users(:correct).questions.first)
  end

  test "should update answer spread_sheet_upload and delete old" do
    sign_in users(:correct)
    file = fixture_file_upload("test_excel.xls", "application/vnd.ms-excel")
    sheet = @spread_sheet_answer.spread_sheet_upload.sheet.url
    # Update answer with new file
    patch :update, id: @spread_sheet_answer, answer: {answer_text: "this is a test answer 2", question_id: users(:correct).questions.first, user_id: @spread_sheet_answer.user.id, spread_sheet_upload_attributes: {sheet: file} }
    @spread_sheet_answer = @spread_sheet_answer.reload # reload to see that update took place in DB
    # Assert paperclip replaced the two files and paperclip deleted the old
    assert_equal("test_excel.xls", @spread_sheet_answer.spread_sheet_upload.sheet_file_name)
    assert_not(File.exists?(sheet), "old upload was not deleted")
    assert_redirected_to question_path(@spread_sheet_answer.question), "failed to update correctly"
  end
  
  test "should update answer and not change spread_sheet_upload" do
    sign_in users(:correct)
    sheet = @spread_sheet_answer.spread_sheet_upload.sheet.path
    sheet_name = @spread_sheet_answer.spread_sheet_upload.sheet_file_name
    sid = @spread_sheet_answer.spread_sheet_upload.id
    # Update answer
    patch :update, id: @spread_sheet_answer, answer: {answer_text: "this is a test answer 2", question_id: users(:correct).questions.first, user_id: @spread_sheet_answer.user.id, spread_sheet_upload_attributes: {id: sid, _destroy: "0"} }
    @spread_sheet_answer = @spread_sheet_answer.reload # reload to see that update took place in DB
    # Assert that the past spread sheet uplaod is the same - no change
    assert_equal(sheet_name, @spread_sheet_answer.spread_sheet_upload.sheet_file_name)
    assert(File.exists?(sheet), "spread sheet upload was deleted")
    assert_redirected_to question_path(@spread_sheet_answer.question), "failed to update correctly"
  end
  
  test "should update answer and delete spread_sheet_upload only" do
    sign_in users(:correct)
    sheet = @spread_sheet_answer.spread_sheet_upload.sheet.path
    sid = @spread_sheet_answer.spread_sheet_upload.id
    # Update answer to delete spread sheet upload
    patch :update, id: @spread_sheet_answer, answer: {answer_text: @spread_sheet_answer.answer_text, question_id: users(:correct).questions.first, user_id: @spread_sheet_answer.user.id, spread_sheet_upload_attributes: {id: sid, _destroy: "1"} }
    @spread_sheet_answer = @spread_sheet_answer.reload # reload to see that update took place in DB
    # Assert spread sheet upload destroyed
    assert_nil(@spread_sheet_answer.spread_sheet_upload)
    assert_nil(SpreadSheetUpload.find_by(id: sid))
    assert_redirected_to question_path(@spread_sheet_answer.question), "failed to update correctly"
  end
  
  test "should mark answer as correct" do
    sign_in @answer.user
    # Mark answer as correct
    patch :update, id: @answer, answer: {correct_answer_id: @answer.question.id}
    assert_equal("Answer marked correct!", flash[:success], "answer not marked correct successfully")
    assert_redirected_to question_path(@answer.question)
  end
  
  test "should not mark answer as correct" do
    sign_in answers(:single_answer).question.user
    # Show that user cannot set their answer to correct (unless question owner)
    patch :update, id: answers(:single_answer), answer: {correct_answer_id: answers(:single_answer).question.id, answer_text: "this is changed text that should not go through"}
    assert_redirected_to "http://test.host/"
    assert_equal("You are not authorized to perform this action.", flash[:alert], "answer marked correct successfully when should not")
    assert(answers(:single_answer).reload.answer_text != "this is changed text that should not go through", "answer text was changed when shouldnt")
  end
  
  test "should destroy answer" do
    # Destroy answer test
    sign_in @answer.user
    question  = @answer.question
    assert_difference('Answer.count', -1) do
      delete :destroy, id: @answer
    end
    assert_redirected_to question_path(question)
  end
  
  test "should destroy answer and attached spreadsheet" do
    # Show that spread sheet is destroyed with answer
    sign_in @spread_sheet_answer.user
    assert_difference('SpreadSheetUpload.count', -1) do
      delete :destroy, id: @spread_sheet_answer
    end
    assert_redirected_to question_path(@spread_sheet_answer.question)
  end
  

  
  test "should filter out script tags from content" do
    sign_in users(:correct)
    # Create answer with script tags
    post :create, answer: {answer_text: "<p>this is a text</p><script>window.alert('sometext')</script><p>this is more text</p>", question_id: users(:correct).questions.first}
    q = Answer.last
    # Assert that no script tags in answer text on create
    assert_not q.answer_text.include?("<script>")
    assert_not q.answer_text.include?("</script>")
    # Assert that no script tags in asnwer text on update
    patch :update, id: q, answer: {answer_text: "<p>this is a not text</p><script>window.alert('sometext')</script><p>this is more text</p>"}
    w = Answer.last
    assert_not w.answer_text.include?("<script>")
    assert_not w.answer_text.include?("</script>")
  end
  
  test "should take to answer show and redirect to answers question show page with reference to its location (anchor)" do
    # show with special redirect
    get :show, id: answers(:correct)
    assert_redirected_to question_path(answers(:correct).question, anchor: "answer-#{answers(:correct).id}")
  end
  
  ##### View Tests #####
  
    test "should show delete flash" do
    sign_in @answer.user
    question  = @answer.question
    delete :destroy, id: @answer
    # Destroy answer and make sure shows correct text.
    assert_equal I18n.t('messages.models.general.delete', model: "Answer"), flash[:success]
  end
  
  test "should show create flash (post)" do
    sign_in users(:correct)
    # Create answer and make sure shows correct flash text
    post :create, answer: { answer_text: "this is a test answer 123456789", question_id: users(:correct).questions.first}
    assert_equal I18n.t('messages.models.general.post', model: "Answer"), flash[:success]
  end
  
  test "should show update flash" do
    sign_in @answer.user
    # Update answer and assert correct flash text
    patch :update, id: @answer, answer: { answer_text: "this is a second test answer 123456", question_id: users(:correct).questions.first, user_id: @answer.user.id}
    assert_equal I18n.t('messages.models.general.update', model: "Answer"), flash[:success]
  end
  
  test "file field show up in answer new" do
    skip("Weird routing makes difficult to test at the moment")
    sign_in @answer.user
    get :new
    assert_select "form input", 4
  end
  
  test "file field show up in answer edit" do
    # fragile replace someday
    sign_in @answer.user
    get :edit, id: @answer.id
    assert_select "form input", 9
  end
  
  test "answer edit display correct" do
    # Assert certan key areas display
    @answer.user = users(:no_posts)
    @answer.save
    sign_in @answer.user
    get :edit, id: @answer.id
    assert_select 'title', "Edit Answer"
    assert_select "a[href=?]", user_path(@answer.question.user.username), count: 1
    assert_select "form", 1
    assert_select 'h3', @answer.question.title
  end
  
  test "answer new display correct" do
    # Assert certain key areas display
    sign_in @answer.user
    post :create,  answer: { answer_text: "", question_id: users(:correct).questions.first}
    assert_select 'title', I18n.t('page_titles.models.general.new', model: "Answer")
    assert_select 'div', "Please review the problems below:"
    assert_select 'span', "can't be blank"
  end
end
