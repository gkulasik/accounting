require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  test "should get index" do
    # Index
    q = Question.last(5)
    # add tags to last 5 questions
    q.each do |t|
      t.tag_list  = Faker::Lorem.words
      t.save
    end
    get :index
    assert assigns(:tags)
    # to check that the default 'most popular' filter is working. 
    # Make sure that the count of each tag is greater than or equal to the next tag count in line (most popular is DESC)
    last_count = assigns(:tags).first.taggings_count
    assigns(:tags).each do |tag|
      assert tag.taggings_count <= last_count
      # assign last_count to the count just compared to move down the line
      last_count = tag.taggings_count
    end
    assert_response :success
  end
  
  test "index filters" do
    # Tests most popular filter, tag count is DESC 
    get :index, {tag_cloud_popular: true}
    tags1 = assigns(:tags)
    last_count1 = tags1.first.taggings_count
    tags1.each do |t|
      assert t.taggings_count <= last_count1
      last_count1 = t.taggings_count
    end
    # Tests the 'newest' filter, which is DESC in ID
    get :index, {tag_cloud_newest: true}
    tags2 = assigns(:tags)
    last_count2 = tags2.first.id
    tags2.each do |t|
      assert t.id <= last_count2
      # Compare ids since they are in order of creation
      last_count2 = t.id
    end
  end
  
  ##### View Tests #####
  
  test "tags index display correct" do
    get :index
    # General display checks
    assert_select 'title', I18n.t('page_titles.models.tag.index')
    assert_select 'p', I18n.t('general.tags.top_tags_info')
  end
  


end
