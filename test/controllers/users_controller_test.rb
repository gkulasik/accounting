require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  setup do
    # Set up test helpers
    request.env['devise.mapping'] = Devise.mappings[:user]
  end
  
  test "should get show/profile page for user" do
    get(:show, {'username' => "pierce777"})
    # assert loads correctly
    assert_not_nil assigns(:user)
    assert_response :success
  end
  
  test "should show edit link for profile owner" do
    sign_in users(:correct)
    # Sign in as owner of profile and assert that see Edit link
    get :show, username: users(:correct).username
    assert_select 'a', "Edit"
  end
  
  test "should NOT show edit link for not profile owner" do
    get :show, username: users(:correct).username
    # Not signed in. No edit link
    assert_select "a[href=?]", "http://test.host/profile/#{users(:correct).username}", count: 0
    sign_in users(:no_posts)
    get :show, username: users(:correct).username
    # Signed in as other user. No edit link
    assert_select "a[href=?]", "http://test.host/profile/#{users(:correct).username}", count: 0
  end
  
  test "user profile page should have certain information" do
    get(:show, {'username' => "pierce777"})
    # Test user page info
    assert_select 'title', I18n.t('page_titles.models.user.show', output_name: "pierce777"), "title is not present"
    assert_select 'td', "Questions: #{assigns(:user).questions.count}", "Number of questions asked not displaying correctly"
    #     puts "count: #{assigns(:user).questions.count}"   # this loop is skipped for some reason. Need to figure out why
#     assigns(:user).questions do |q|
#       assert_select "a", "#{q.title}"
#       puts "count: #{assigns(:user).questions.count}"
#     end
#     assigns(:user).answers do |a|
#       assert_select "a", "#{a.q.title}"
#     end
  end



##### View Tests #####
  test "user show display correct" do
    # test other visuals - like that tabs show up
    get :show, {username: users(:correct).username}
    assert_select 'h2', users(:correct).username
    assert_select "a", "Questions"
    assert_select "a", "Answers"
    assert_select "a", "Activity"
  end
  


end
