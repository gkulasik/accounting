require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  
  
  test "should create a comment for a question" do
    # SImply create comment for question assert it exists
    sign_in users(:correct)
    assert_difference('Comment.count') do
      post :create, comment: {content: "this is a test comment, question", commentable_id: users(:correct).questions.first.id, commentable_type: users(:correct).questions.first.class.name}
    end
      assert_redirected_to question_path(users(:correct).questions.first)
  end
    
    test "should create a comment for an answer" do
    # Simply create comment for answer assert it exists
    sign_in users(:correct)
    assert_difference('Comment.count') do
      post :create, comment: {content: "this is a test comment, answer", commentable_id: answers(:correct).id, commentable_type: answers(:correct).class.name}
    end
      assert_redirected_to question_path(answers(:correct).question)
  end
  
  test "should destroy a comment" do
    # Delete comment
    sign_in comments(:correct).user
    comments(:correct).commentable = questions(:correct)
    comments(:correct).save
    assert_difference('Comment.count', -1) do
      delete :destroy, id: comments(:correct)
    end
    assert_redirected_to question_path(questions(:correct))
  end


##### View Tests #####
  test "should show delete flash" do
    # Assert correct flash display on delete
    sign_in comments(:correct).user
    delete :destroy, id: comments(:correct).id
    assert_equal I18n.t('messages.models.general.delete', model: "Comment"), flash[:success]
  end
  
  test "should show create flash" do
    # Assert correct flash display on create
    sign_in users(:correct)
    post :create, comment: {content: "this is a test comment, question 12345", commentable_id: users(:correct).questions.first.id, commentable_type: users(:correct).questions.first.class.name}
    assert_equal I18n.t('messages.models.general.post', model: "Comment"), flash[:success]
  end
  
end
