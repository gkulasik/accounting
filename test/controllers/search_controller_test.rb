require 'test_helper'

class SearchControllerTest < ActionController::TestCase
  setup do
    # Rebuild the search DB cause seems with tests it does not build as expected
    PgSearch::Multisearch.rebuild(Answer)
    PgSearch::Multisearch.rebuild(Question)
  end
  
  test "should not get results (no search param)" do
    # Empty search string returns no results
    get(:results)
    # when searching and get no results - put correct flash up
    assert_equal I18n.t('messages.search.no_results'), flash[:alert], "did not redirect with 'No results.' flash"
    assert_response :redirect
  end

  test "should get a result" do
    # Tests search finding things it should
    # Search string might return some results
    get(:results, {'search_query' => 'hello'})
    assert_not_nil assigns(:results)
    # only one result should some from this search
    assert_equal assigns(:results).count, 1, "found more than it should have"
    assert_response :success
  end
  
  test "should get results (find more than one)" do
    # Tests search finding things it should
    # Search string might return some results
    get(:results, {'search_query' => 'question'})
    assert_not_nil assigns(:results)
    # Only three results shuld come back with this search
    assert_equal 3, assigns(:results).count, "did not find all the results it was supposed too"
    assert_response :success
  end
  
  test "should get results that are a mix of questions and answers" do
    # Tests search finding things it should
    # Search string might return some results
    get(:results, {'search_query' => 'this is a test'})
    assert_not_nil assigns(:results)
    # Make sure search result returns back both answers and questions (both are searched)
    question = false
    answer = false
    assigns(:results).each do |r|
      # if found a question mark it off as found 
        if r.searchable_type == "Question"
          question = true
        end
        # if found an answer mark it off as found
        if r.searchable_type == "Answer"
          answer = true
        end
    end
    assert(question)
    assert(answer)
    # search should have 6 results
    assert_equal 6, assigns(:results).count, "did not find all the results it was supposed too"
    assert_response :success
  end
  
  test "on create of a question PgSearch count should increase" do
    # Make sure that PGsearch doc is created with question
    assert_difference('PgSearch::Document.count') do
      Question.create(title: "123456789 123456789 123456789 123456789",  question_text: "this is quesiton text. This is text for a question", user: users(:correct), tag_list: "tag1, tag2")
    end
    get(:results, {'search_query' => '123456789'})
    assert_not_nil assigns(:results)
    #  When search on this unique title shoudl find it, and only it. 
    assert_equal assigns(:results).count, 1
    assert_response :success
  end
  
  
  test "on create of an answer PgSearch count should increase" do
    # Make sure that PGsearch doc is created with answer
    assert_difference('PgSearch::Document.count') do
      Answer.create(answer_text: "987654321 987654321 987654321", question_id: users(:correct).questions.first.id, user_id: users(:correct).id)
    end
    get(:results, {'search_query' => '987654321 987654321 987654321'})
    assert_not_nil assigns(:results)
    #  When search on this unique content should find it, and only it. 
    assert_equal assigns(:results).count, 1
    assert_response :success
  end
  
  test "pagination of search results" do
    # check pagination of search results
    # Cut all total search into 2 pages (for simplcity)
   count =  PgSearch.multisearch("this is a test question text").count
   count1 = count/2
  # search per page and then assert that many results came back
   first_search = PgSearch.multisearch("this is a test question text").page(1).per(count1)
   assert(first_search.count == count1, "1st search returned more than count1")
   second_search = PgSearch.multisearch("this is a test question text").page(2).per(count1)
  # second search might have slighlty less if count is odd
   assert(second_search.count <= count1, "2nd search returned more than count1")
   full_search = first_search + second_search
  # make sure all records returned are unique and no duplicates on either page.
   assert(full_search.count == full_search.uniq.count, "not returning all records, some are duplicates")
  end
  

  ##### View Tests #####
  
  test "search results display correct" do
    # assert results page looks a certain way
    get :results, {search_query: "question"}
    q = PgSearch.multisearch("question").first.searchable
    # Title and a few key divs
    assert_select 'title', I18n.t('page_titles.search_results')
    assert_select 'div.question_results_tags'
    assert_select 'div.question_poster_info_'+q.id.to_s
  end
end
