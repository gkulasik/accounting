require 'test_helper'

class QuestionsControllerTest < ActionController::TestCase
  
  setup do
    # Default question
    @question = questions(:correct)
    # a brand new question without any baggage
    @fresh_question = Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user: users(:correct), tag_list: "tag1, tag2")
    # Create a file for a spread sheet. 
    file = fixture_file_upload("test_csv.csv", "text/csv")
    # Question with spread sheet attachment
    @spread_sheet_question = Question.create(duplicate: nil, question_text: "b"*32, title: "ab"*15, user: users(:correct), tag_list: "tag1, tag2", spread_sheet_upload_attributes: {sheet: file})
  end

  # teardown do
  #   # Use this to remove the /questions folder that is created when tests are run as Paperclip does not transmit to S3 during tests
  #   if File.exists?("#{Rails.root}/question")
  #   FileUtils.remove_dir "#{Rails.root}/question", false
  #   end
  # end
  
  
  test "should get index" do
    sign_in @question.user
    # Get index assert questions assigned
    get :index
    assert_response :success
    assert_not_nil assigns(:questions)
  end
  
   test "pagination of index" do
    10.times do |i|
      # Create 10 questions for pagination test
      Question.create(title: "#{i}a"*15, question_text: "#{i}b"*17, user: users(:correct), tag_list: "tag1")
    end
    all_questions = []
    count = Question.count
    loop_num = 1
    # As long as there are still questions in the count keep looping
    while(count > 0)
    # Get each page
      get(:index, page: loop_num)
      assert_not_nil assigns(:questions)
      # Assert # of questions per page is 8
      assert assigns(:questions).count <= 8
      # Put each question into an array to make sure each page returns unique results (does not return same question more than once for display)
      assigns(:questions).each do |q|
        all_questions.push(q.id)
      end
      # Use loop number to get the right page of questions
      loop_num=loop_num+1
      # subtract 8 from count (each page has 8 per the tkaminari test config) 
      count = count-8
    end
    # assert that all questions were returned (counts equal) and that all are uniq.
    assert_equal(all_questions.count, Question.count)
    assert_equal(all_questions.count, all_questions.uniq.count)
  end
  
  test "filter of questions" do
    # Test that the link will put the param in the url and that with_tags will keep both params in the url. Dont test if the filters actually work
    # already tested in model.
    # Tests each filter to see that it returns a valid response. 
    get(:index, "unanswered" => true)
    assert_response :success
    
    get(:index, "recently_updated" => true)
    assert_response :success
    
    get(:index, "newest" => true)
    assert_response :success
    
    # Test with_tags and multiple params
    q1 =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
    get(:index, "with_tags" => "tag1")
    assert_response :success
    
    get(:index, "unanswered" => true, "with_tags" => "tag1")
    assert_response :success
    
    get(:index, "newest" => true, "with_tags" => "tag1")
    assert_response :success
    
    get(:index, "recently_updated" => true, "with_tags" => "tag1")
    assert_response :success
  end
  
  test "should get new" do
    # Get new
    sign_in @question.user
    get :new
    assert_response :success
  end

  test "should create question" do
    # Create questions
    sign_in users(:correct)
    assert_difference('Question.count') do
      post :create, question: {duplicate: nil, question_text: "a"*31, title: "b"*16, tag_list: "tag1, tag2"}
    end
    assert_redirected_to question_path(assigns(:question))
  end
  
  
  test "should not create question with invalid attachment type" do
    sign_in users(:correct)
    # Create array of invalid attachment types to try and spoof the attachment valiidator.
    file_array = ["bunny.jpeg", "notXLSX.txt", "test_WORD.docx", "notXLSX.xls"]
    type_array = ["image/jpeg", "text/text", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-excel"]
    # For each type of invalid file try to create and assert the question is not created
    3.times do |i|
      file = fixture_file_upload(file_array[i], type_array[i]) # Tests multiple file types in a loop to make sure that none of them pass this test.
      # make sure no spread sheet or question are created. 
      before = SpreadSheetUpload.count
      assert_no_difference('Question.count') do
        post :create, question: {duplicate: nil, question_text: "a"*32, title: "b"*17, tag_list: "tag1, tag2", spread_sheet_upload_attributes: {sheet: file}}
        assert_not_empty assigns(:question).errors.messages
      end
      after = SpreadSheetUpload.count
      assert_equal(before, after, "attached spread sheet created when should not be") # basically assert_no_difference("SpreadSheetUpload.count")
      file.close
      assert_response :success # successfully return to the edit page
    end
  end
  
  test "should create question with valid attachment type" do
    sign_in users(:correct)
    # Use 3 caliv attachment types (all the accepted types) and make sure the question and spreadsheet are created 
    file_array = ["test_csv.csv", "test_excel - Copy.xlsx", "test_excel.xls"]
    type_array = ["text/csv", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel"]
    3.times do |i|
      file = fixture_file_upload(file_array[i], type_array[i]) # Tests multiple file types in a loop to make sure that none of them pass this test.
      before = SpreadSheetUpload.count
      # Assert change in count of both spread sheet and question
      assert_difference('Question.count') do
       post :create, question: {duplicate: nil, question_text: "a"*32+file_array[i], title: "b"*17+file_array[i], tag_list: "tag1, tag2", spread_sheet_upload_attributes: {sheet: file}}
      # no errors
       assert_empty assigns(:question).errors.messages
      end
      after = SpreadSheetUpload.count
      assert_equal(before+1, after, "attached spread sheet NOT created when should be") # basically assert_difference("SpreadSheetUpload.count")
      file.close
      assert_redirected_to assigns(:question) # successfully return to the edit page
    end
  end

  test "should show question" do
    get :show, id: @question
    assert_response :success
  end

  test "should get edit" do
    sign_in @question.user
    get :edit, id: @question
    # Edit question
    assert_response :success
  end

  test "should update question" do
    sign_in @question.user
    # Update question
    patch :update, id: @question, question: { duplicate: @question.duplicate, question_text: @question.question_text, title: @question.title, user_id: users(:correct).id }
    assert_redirected_to question_path(@question), "failed to update correctly"
  end
  
  test "should update questions spread_sheet_upload and delete old" do
    sign_in @spread_sheet_question.user
    # Create file for upload test
    file = fixture_file_upload("test_excel.xls", "application/vnd.ms-excel")
    # Save old quesiton spread sheet for compare
    sheet = @spread_sheet_question.spread_sheet_upload.sheet.url
    # update quesiton
    patch :update, id: @spread_sheet_question, question: { duplicate: @spread_sheet_question.duplicate, question_text: @spread_sheet_question.question_text, title: @spread_sheet_question.title, user_id: users(:correct).id, spread_sheet_upload_attributes: {sheet: file} }
    @spread_sheet_question = @spread_sheet_question.reload # reload to see that update took place in DB
    # Assert the file names are the same
    assert_equal("test_excel.xls", @spread_sheet_question.spread_sheet_upload.sheet_file_name)
    # assert file deleted
    assert_not(File.exists?(sheet), "old upload was not deleted")
    assert_redirected_to question_path(@spread_sheet_question), "failed to update correctly"
  end
  
  test "should update question and not change spread_sheet_upload" do
    sign_in @spread_sheet_question.user
    #  gather spread sheet attributes
    sheet = @spread_sheet_question.spread_sheet_upload.sheet.path
    sheet_name = @spread_sheet_question.spread_sheet_upload.sheet_file_name
    sid = @spread_sheet_question.spread_sheet_upload.id
    # update question
    patch :update, id: @spread_sheet_question, question: { duplicate: @spread_sheet_question.duplicate, question_text: "this is question text lalalalalallalalala", title: @spread_sheet_question.title, user_id: users(:correct).id, spread_sheet_upload_attributes: {id: sid, _destroy: "0"} }
    @spread_sheet_question = @spread_sheet_question.reload # reload to see that update took place in DB
    # test that spread sheet is same as before (not changed or lost)
    assert_equal(sheet_name, @spread_sheet_question.spread_sheet_upload.sheet_file_name)
    assert(File.exists?(sheet), "spread sheet upload was deleted")
    assert_redirected_to question_path(@spread_sheet_question), "failed to update correctly"
  end
  
  test "should update question and delete spread_sheet_upload" do
    sign_in @spread_sheet_question.user
    # gather spread sheet attributes
    sheet = @spread_sheet_question.spread_sheet_upload.sheet.path
    sid = @spread_sheet_question.spread_sheet_upload.id
    # update question (and delete spread sheet)
    patch :update, id: @spread_sheet_question, question: { duplicate: @spread_sheet_question.duplicate, question_text: @spread_sheet_question.question_text, title: @spread_sheet_question.title, user_id: users(:correct).id, spread_sheet_upload_attributes: {id: sid, _destroy: "1"} }
    @spread_sheet_question = @spread_sheet_question.reload # reload to see that update took place in DB
    # assert no spread sheet 
    assert_nil(@spread_sheet_question.spread_sheet_upload)
    assert_nil(SpreadSheetUpload.find_by(id: sid))
    assert_redirected_to question_path(@spread_sheet_question), "failed to update correctly"
  end

  test "should destroy question" do
    sign_in @fresh_question.user
    # Destroy question
    assert_difference('Question.count', -1) do
      delete :destroy, id: @fresh_question
    end
    assert_redirected_to questions_path
  end
  
  test "should destroy question and attached spreadsheet" do
    sign_in @spread_sheet_question.user
    # assert spread sheet destroyed with question
    assert_difference('SpreadSheetUpload.count', -1) do
      delete :destroy, id: @spread_sheet_question
    end
    assert_redirected_to questions_path
  end
  
  
  test "should filter out script tags from content" do
     sign_in users(:correct)
    # create question with script tags
      post :create, question: {duplicate: nil, question_text: "<p>this is a text</p><script>window.alert('sometext')</script><p>this is more text</p>", title: "123456789123456789", tag_list: "tag1, tag2"}
    q = Question.find_by(title: "123456789123456789")
    # assert after save that script tags were filtered out
    assert_not q.question_text.include?("<script>")
    assert_not q.question_text.include?("</script>")
    # update question with script tags
     patch :update, id: q, question: {duplicate: nil, question_text: "<p>this is a not text</p><script>window.alert('sometext')</script><p>this is more text</p>", title: "123456789123456789"}
    w = Question.find_by(title: "123456789123456789")
    # assert script tags filtered out. 
    assert_not w.question_text.include?("<script>")
    assert_not w.question_text.include?("</script>")
  end
 
 
##### View Tests #####
  test "should show delete flash" do
    # check delete flash message
    sign_in @fresh_question.user
    delete :destroy, id: @fresh_question
    assert_equal I18n.t('messages.models.general.delete', model: "Question"), flash[:success]
  end
  
  test "should show create flash" do
    sign_in users(:correct)
    # check create flash
    post :create, question: {duplicate: nil, question_text: "a"*32, title: "b"*18, tag_list: "tag1, tag2"}
    assert_equal I18n.t('messages.models.general.post', model: "Question"), flash[:success]
  end
  
  test "should show update flash" do
    sign_in @question.user
    # Check update flash
    patch :update, id: @question, question: { duplicate: @question.duplicate, question_text: @question.question_text, title: @question.title, user_id: users(:correct).id }
    assert_equal I18n.t('messages.models.general.update', model: "Question"), flash[:success]
  end
  
  test "file field show up in question new" do
    # fragile should replace in future
    # checks number of form inputs to assert file field is showing up
    sign_in @question.user
    get :new
    assert_select "form input", 5
  end
  
  test "file field show up in question edit" do
    #fragile should replace in future
    sign_in @question.user
    # checks number of form inputs to assert file field is showing up
    get :edit, id: @question
    assert_select "form input", 6
  end
  
  test "question edit display correct" do
    sign_in @question.user
    # Asserts certain elements are displayed correctly. Title, Heading, form
    get :edit, id: @question.id
    assert_select 'title', I18n.t('page_titles.models.general.edit', model: "Question")
    # heading matchs page title
    assert_select 'h1', I18n.t('page_titles.models.general.edit', model: "Question")
    assert_select "form", 1
  end
  
  test "question new display correct" do
    sign_in @question.user
    get :new
    # Assert elements of new page display correctly. Title, heading, form
    assert_select 'title', I18n.t('page_titles.models.question.ask')
    # heading is same as title
    assert_select 'h2', I18n.t('page_titles.models.question.ask')
    assert_select 'form', 1
  end
  
  test "question show display correct" do
    sign_in users(:no_posts)
    get :show, id: @question.id
    # question show dispay correctly. 
    assert_select 'h3', @question.title #page title is question title
    # links to questions user profile and no link to edit question unless signed in
    assert_select "a[href=?]", user_path(@question.user.username)
    assert_select "a[href=?]", edit_question_path(@question.id), 0
    # answer form
    assert_select "form"
    sign_out users(:no_posts)
    sign_in @question.user
    get :show, id: @question.id
    # sign in as correct user - assert edit link exists.
    assert_select "a[href=?]", edit_question_path(@question), 1
  end
  
  test "question show display security for answers" do
    get :show, id: @question.id
    # assert that quesiton owner cannot see answers edit/delete links - only answer owner can see them
    assert_select "a[href=?]", edit_answer_path(@question.answers.first.id), 0
    assert_select "a[data-method=?]", "delete", 0
    
    a = @fresh_question.answers.build(answer_text: "this is an answer that does not belong to question owner", user: users(:no_posts))
    a.save
    a = a.reload
    sign_in @fresh_question.user
    get :show, id: @fresh_question.id
    # no answer edit links for question owner
    assert_select "a[href=?]", edit_answer_path(a), 0
    assert_select "a[href=?]", answer_path(a), 0
    # assert a mark answer as correct link appears as question owner
    assert_select "a", text: "Mark answer as correct"
    sign_out @fresh_question.user
    
    sign_in a.user
    get :show, id: @fresh_question.id
    # answer owner can see edit and delete links but not mark answer as correct
    assert_select "a[href=?]", edit_answer_path(a)
    assert_select "a[href=?]", answer_path(a)
    assert_select "a", text: "Mark answer as correct", count: 0
  end
  
  test "question index display correct" do
    get :index
    # index is home page so assert all the main links are present 
    assert_select 'h1', "Accounting Studies" # title
    assert_select "a[href=?]", root_path
    assert assigns(:questions)
    assert_select "a[href=?]", question_path(assigns(:questions).first) # questions listing
    assert_select "a", "Questions" # all links
    assert_select "a", "Calculator"
    assert_select "a", "About us"
    assert_select "a", "Help"
    assert_select "a", "Log in"
    assert_select "a", "Sign up"
    assert_select "a", "Ask a Question"
  end
  
  test "comment info displays correctly" do
    skip("Cant find span tag for some reason")
    # supposed to show that comment tooltip displaying correctly
    get :show, id: Question.first.id
    assert_select ".tooltip", I18n.t("general.comment.post_info")
  end
  
  test "comment can comment displays correctly" do
    get :show, id: Question.first.id
    # when not signed in the text area and button should be disabled.
    assert_select "textarea[placeholder=?]", I18n.t('general.comment.can_not_post')
    assert_select "textarea[disabled=?]", "disabled"
    assert_select "input[class=?]", "small button tiny disabled"
    sign_in users(:correct)
    get :show, id: Question.first.id
    # once signed in may be enabled.
    assert_select "textarea[placeholder=?]", I18n.t('general.comment.can_post')
    assert_select "textarea[disabled]", count: 0
    assert_select "input[class=?]", "small button tiny "
  end
end
