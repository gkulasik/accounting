ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'fileutils'
require 'pundit'
require 'truncate_html'


class ActiveSupport::TestCase
  include Pundit
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  
  # Helper that allows easy reassignment of one attribute
  def change_one_attribute(object, attribute, new_value)
    object[attribute] = new_value
    object
  end
  
  # Should use ! for actions in tests to see where things fail rather then silent fail
 
end

class ActionController::TestCase
  # added for access to devise helpers in all controller tests
  include Devise::TestHelpers
  # added for ability to use t() method for getting text from en.yml file
  include I18n
  
  # Post test clean up
  teardown do
    # Use this to remove the /questions folder that is created when tests are run as Paperclip does not transmit to S3 during tests
    if File.exists?("#{Rails.root}/question")
     FileUtils.remove_dir "#{Rails.root}/question", false
    end
    
     # Use this to remove the /answers folder that is created when tests are run as Paperclip does not transmit to S3 during tests  
    if File.exists?("#{Rails.root}/answer")
     FileUtils.remove_dir "#{Rails.root}/answer", false
    end
  end
end

class ActionView::TestCase
  # added to test if truncate works correctly (also so that tests could run)
  include TruncateHtmlHelper
  # added so that could test visual things in view tests that are dependent on current_user
  def current_user
    users(:correct)
  end
end