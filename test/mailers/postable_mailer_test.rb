require 'test_helper'

class PostableMailerTest < ActionMailer::TestCase
  test "comment on answer email" do
    # Send the email, then test that it got queued
    email = PostableMailer.new_comment_on_post_email(answers(:correct), comments(:correct_a)).deliver_now
    assert_not ActionMailer::Base.deliveries.empty?
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['accountingstudies@rampitupdevelopment.com'], email.from
    # Sends to the owner of the answer which is correct user
    assert_equal ['correct_user@example.com'], email.to
    assert_equal I18n.t('site_name')+": New comment on answer", email.subject
  end
  
  test "comment on question email" do
    # Send the email, then test that it got queued
    email = PostableMailer.new_comment_on_post_email(questions(:correct), comments(:correct)).deliver_now
    assert_not ActionMailer::Base.deliveries.empty?
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['accountingstudies@rampitupdevelopment.com'], email.from
    # Sends to the owner of the question which is correct user
    assert_equal ['correct_user@example.com'], email.to
    assert_equal I18n.t('site_name')+": New comment on question", email.subject
  end
  
  test "post answer to question email" do
    # Send the email, then test that it got queued
    email = PostableMailer.new_answer_on_post_email(questions(:correct), answers(:correct)).deliver_now
    assert_not ActionMailer::Base.deliveries.empty?
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['accountingstudies@rampitupdevelopment.com'], email.from
    # Sends to the owner of the question which is correct user
    assert_equal ['correct_user@example.com'], email.to
    assert_equal I18n.t('site_name')+": New answer on question", email.subject
  end
end
