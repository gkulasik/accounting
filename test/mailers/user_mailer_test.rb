require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  
  test "welcome email" do
    # Send the email, then test that it got queued
    email = UserMailer.welcome_email(users(:correct).id).deliver_now
    assert_not ActionMailer::Base.deliveries.empty?
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['accountingstudies@rampitupdevelopment.com'], email.from
    assert_equal ['correct_user@example.com'], email.to
    assert_equal "Welcome to "+I18n.t('site_name')+"!", email.subject
  end
end
