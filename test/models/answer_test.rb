require 'test_helper'

class AnswerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should not save answer without a question" do
    # check validation answer must have question 
    assert_not change_one_attribute(answers(:correct), "question_id", nil).save, "saved an answer without a question"
  end
  
  test "should not save an answer without a user" do
    # check validation that answer must have user
    assert_not change_one_attribute(answers(:correct), "user_id", nil).save, "saved an answer without a user"
  end
  
  test "should save a correct answer" do
    # check that will save a correctly filled in answer
    assert answers(:correct).save, "did not save a correct answer"
  end
  test "should not save an answer without content" do
    # check validatio that cannot save answer without content
    assert_not change_one_attribute(answers(:correct), "answer_text", nil).save, "saved an answer without answer text (content)"
  end
  
  test 'of the correct? method' do
    # set correct answer
    questions(:correct).correct_answer = answers(:correct)
    # assert that returns correct result  - answer.correct? returns the question it is the correct answer for or false
    assert_equal questions(:correct), answers(:correct).correct?, "correct method in Answer is not returning correct question"
    assert_not answers(:one).correct?, "this answer is not correct, should return false"
  end
  
  
  test "return spread sheet upload or new" do
    # set up for an answer to have a spreadsheet
     extend ActionDispatch::TestProcess # needed for fixture_file_upload
     file = fixture_file_upload("test_csv.csv", "text/csv")
    # set answers spreadsheet/update answer
     spread_sheet_answer = answers(:one)
     spread_sheet_answer.update(spread_sheet_upload_attributes: {sheet: file})
     # find an answer without a spreadsheet
     ans_without_spreadsheet = nil
    Answer.all.each do |a|
      if(a.spread_sheet_upload == nil)
        # find answer with no spread sheet
        ans_without_spreadsheet = a
        break
      end
    end
    # find an answer with a spreadsheet - fresh (to make sure it is saved correctly and tat the get_spread_sheet_upload works)
    ans_with_spreadsheet = SpreadSheetUpload.where(attachable_type: "Answer").first.attachable
    # test that results are as expected
    assert_equal(ans_without_spreadsheet.get_spread_sheet_upload.id, nil)
    assert_not_equal(ans_with_spreadsheet.get_spread_sheet_upload.id, nil)
    
    ans_with_spreadsheet.destroy
  end
  
  test "on destroy answer should be deleted and all dependent records" do
    extend ActionDispatch::TestProcess # needed for fixture_file_upload
    a = Answer.new(answer_text: "balnk text for answer", question_id: questions(:correct).id, user_id: users(:correct).id)
    a.save
    a.reload
    Comment.create(content: "test content for destroy", user_id: users(:correct).id, commentable: a)
    
    assert_difference('Comment.count', -1) do
      a.destroy
    end
    a = Answer.new(answer_text: "balnk text for answer", question_id: questions(:correct).id, user_id: users(:correct).id)
    a.save
    PgSearch::Multisearch.rebuild(Answer)
    assert_difference('PgSearch::Document.count', -1) do
      a.destroy
      PgSearch::Multisearch.rebuild(Answer)
    end
     file = fixture_file_upload("test_csv.csv", "text/csv")
    # set answers spreadsheet/update answer
     spread_sheet_answer = Answer.new(answer_text: "balnk text for answer", question_id: questions(:correct).id, user_id: users(:correct).id)
     spread_sheet_answer.save
     spread_sheet_answer.update(spread_sheet_upload_attributes: {sheet: file})
    
    assert_difference('SpreadSheetUpload.count', -1) do
      spread_sheet_answer.destroy
    end
    
    
  end
  
end
