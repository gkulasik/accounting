require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  test "should not save a comment without a user" do
    # check validations that comment must have user
    assert_not change_one_attribute(comments(:correct), "user_id", nil).save, "saved a comment without user"
  end
  
  test "should not save a comment without content" do
    # check validation that content cannot be empty
    assert_not change_one_attribute(comments(:correct), "content", nil).save, "saved a comment without content"
  end
  
  test "should not save a comment without a commentable associated" do
    # check validation that comment must have parent
    assert_not change_one_attribute(change_one_attribute(comments(:correct), "commentable_id", nil), "commentable_type", nil).save, "saved a comment without a commentable associated"  
  end
  
  test "should save a correct comment" do
    # check that valid comments are saved without issue
    assert comments(:correct).save, "did not save a correct comment for a question"
    assert comments(:correct_a).save, "did not save a correct comment for an answer"
  end
  
  test "default_order scope should produce ascending created at (oldest to newest)" do
    # check default sort
    3.times do |i|
      # create comments for sort
      Comment.create!(content: "#{i}", user: users(:correct), commentable: questions(:correct))
    end
    # get last three comments
    comments = Comment.all.default_order.last(3)
    # assert that the comments are in order (1-3) - check content
    assert comments.first.content == "0", "comments 0 not appearing in the correct order"
    assert comments.second.content == "1", "comments 1 not appearing in the correct order"
    assert comments.third.content == "2", "comments 2 not appearing in the correct order"
  end
  
  test "of auto html conversion plus protection" do
    # Test autohtml to make sure doing job
    c = comments(:correct)
    c.content = "this is text, www.google.com"
    c.save
    # save a comment with a link. check that the <a> tag is added around it to link to the link (new tab).
    assert_equal "this is text, <a href=\"http://www.google.com\" target=\"_blank\" rel=\"nofollow\">www.google.com</a>", c.content_html, "auto_html is not converting links correctly"
    c.content = "text, <script></script>"
    c.save
    # change the content of the comment to have script tags, make sure that gem is escaping and not allowing script to be executed. 
    assert_equal "text, &lt;script&gt;&lt;/script&gt;", c.content_html, "auto_html is not escaping script tags"
  end
  
  test "should delete a comment when its commentable is destroyed" do
    # create answer and comment that is linked to answer.
    a = Answer.create(answer_text: "this iis asnwer content hahah ha ah ha ha ha  test test test", user: users(:correct), question: questions(:correct))
    c = Comment.create(content: "this is a test comment that should be deleted.", user: users(:no_posts), commentable: a)
    # Assert comment exists to make sure test is valid
    assert_equal(true, Comment.exists?(c.id), "comment doesnt exists when should")
    # delete answer and then test that comment is also deleted.
    a.destroy
    # assert comment does not exist
    assert_equal(false, Comment.exists?(c.id), "comment not destroyed when should be (commentable destroyed)")
    # create question and comment that is linked to answer.
    q = Question.create(title: "this questionw ill be delted", question_text: "this iis question content hahah ha ah ha ha ha  test test test", user: users(:correct), tag_list: "tag test")
    c = Comment.create(content: "this is a test comment that should be deleted 2.", user: users(:no_posts), commentable: q)
    # Assert comment exists to make sure test is valid
    assert_equal(true, Comment.exists?(c.id), "comment doesnt exists when should")
    # delete answer and then test that comment is also deleted.
    q.destroy
    # assert comment does not exist
    assert_equal(false, Comment.exists?(c.id), "comment not destroyed when should be (commentable destroyed)")
  end
  
end
