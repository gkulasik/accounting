require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  
  test "question must have a title" do
    # check validation that question must have 
    assert_not change_one_attribute(questions(:correct), "title", nil).save, "saved a quesiton without a title"
  end
  test "save a correctly filled in question" do
    # check that valid question is saved
    assert questions(:correct).save, "did not save a correctly filled in question"
  end
  test "question must have content" do
    # check validation that question must have title
    assert_not change_one_attribute(questions(:correct), "question_text", nil).save, "saved a question without question text (content)"
  end
  test "do not save an empty question" do
    # check that an empty question cannot be saved
    assert_not Question.new.save, "saved an empty question"  
  end
  test "do not save a question without a user" do
    # check validation that question must have a user
    assert_not change_one_attribute(questions(:correct), "user_id", nil).save, "saved a question without a user"
  end
  
  test "to-param should output id and question title" do
    q = Question.last
    # test params has title and id of question
    assert q.to_param.include?("#{q.id}"), "the to_param method does not include the questions id"
    assert q.to_param.include?("#{q.title.parameterize}"), "the to_param method does not include the quesitons title"
  end
  
  test "should not save question (title and question text too short)" do
    
    q = Question.new(duplicate: nil, question_text: "a"*31, title: "b"*14, user_id: users(:correct).id, tag_list: "tag1, tag2")
    assert_no_difference('Question.count') do
      # since title is too sort should not save
      q.save
    end
    q.title = "b"*16
    q.question_text = "b"*29
    assert_no_difference('Question.count') do
      # since content is too sort should not save
      q.save
    end
    q.title = "b"*16
    q.question_text = "b"*31
    assert_difference('Question.count') do
      # both content and title are long enough so should save
      q.save
    end
  end
  
  test "answered method" do
    q = questions(:correct)
    # Assert the quesiton is not answered, method returns false
    assert_not q.answered?, "unanswered question is labled as answered"
    # Assign a correct answer
    q.correct_answer = q.answers.first
    q.save
    # now should return true.
    assert q.answered?, "answered question not returning true"
  end
  
  test "correct_answer relationship" do
    q = questions(:correct)
    # Test the correct_answer relationship, when no answer assigned then should not have a correct answer 
    assert_equal nil, q.correct_answer, "unanswered question has a correct answer"
    # assign correct answer
    a = q.answers.first
    q.correct_answer = a
    # now should have correct answer 
    assert_equal a, q.correct_answer, "correct answer and expected do not match"
  end
  
  
  test "newest scope" do
    # Create two extra questions which are clearly seperate in time.
    q = nil
    2.times do 
      # q will be assigned to the last question made (newest)
     q =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1, tag2")
      sleep(0.5)
    end
    # get newest quesitons 
    questions = Question.newest(true)
    # assert that the first question in questions is q
    assert_equal(questions.first, q, "newest question not first in newest scope")
    # assign the newest question time to compare too
    last_date = questions.first.created_at
    questions.each do |q|
      # for each quesiton assert that the created at of the q is less than or equal to the last time/date recorded
      assert(q.created_at <= last_date, "Not in newest order")
      # assign the new latest date to the next question in the list to keep the comparison going
      last_date = q.created_at
    end
  end
  
  test "unanswered scope" do
    # create 2 quesitons one answered and one unanswered. No other questions are answered.
    q1 =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1, tag2")
    q1.correct_answer = answers(:correct)
    q1.save
    q2 =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1, tag2")
    # Get the un answered quesitons
    questions = Question.unanswered(true)
    # assert that none of the questions are answered. more specifically assert that the 'answered' question does not exist in the set of un answered questions
    questions.each do |q|
      if q == q1
        flunk("Have answered question in unanswered scope")
      end
    end
  end
  
  test "recenty updated scope" do
     # Create two extra questions which are clearly seperate in time. (update will match created at.)
    2.times do 
      Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1, tag2")
      sleep(0.5)
    end
    # update a question for good measure 
    q1 = Question.newest(true).last
    q1.update_attribute(:title, "c"*17)
    # get the recently updated questions
    questions = Question.recently_updated(true)
    # assert the most recently updated question is first (q1)
    assert_equal(questions.first, q1, "the most recently updated question is not first in recently updated")
    # assign the newly updated q time to var for testing
    last_update = questions.first.updated_at
    questions.each do  |q|
      # check that each question updated at time is less than or equal to the quesiton before it
      assert(q.updated_at <= last_update, "Not in recently updated order")
      # re assign to keep testing down the line
      last_update = q.updated_at
    end
  end
  
  test "with tags scope" do
    # create two quesitons with unique tags
    q1 =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
    q2 =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag2")
    # assert that with_tags scope finds the question with the tag (and there is only one)
    assert(Question.with_tags("tag1").count == 1, "more tagged than should be 'tag1'")
    assert(Question.with_tags("tag2").count == 1, "more tagged than should be 'tag2'")
    # then assert that the one that is found is the questions created above
    assert(Question.with_tags("tag1").first == q1, "'tag1' question not returned")
    assert(Question.with_tags("tag2").first == q2, "'tag2' question not returned")
  end
  
  test "return spread sheet upload or new" do
    # set up for an question to have a spreadsheet
     extend ActionDispatch::TestProcess
     file = fixture_file_upload("test_csv.csv", "text/csv")
     spread_sheet_question = questions(:correct)
     spread_sheet_question.update(spread_sheet_upload_attributes: {sheet: file})
     # find an question without a spreadsheet
     q_without_spreadsheet = nil
    Question.all.each do |q|
      if(q.spread_sheet_upload == nil)
        q_without_spreadsheet = q
        break
      end
    end
    # find a question with a spreadsheet
    q_with_spreadsheet = SpreadSheetUpload.where(attachable_type: "Question").first.attachable
    # test that creates new when should and otherwise returns spreadsheet
    assert_equal(q_without_spreadsheet.get_spread_sheet_upload.id, nil)
    assert_not_equal(q_with_spreadsheet.get_spread_sheet_upload.id, nil)
  end
  
  test "on destroy question should delete all dependent records" do
    extend ActionDispatch::TestProcess # needed for fixture_file_upload
    
    q =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
    a = Answer.create(answer_text: "balnk text for answer", question_id: q.id, user_id: users(:correct).id)
    assert_difference('Answer.count', -1) do
      q.destroy
    end
    
    q =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
    Comment.create(content: "test content for destroy", user_id: users(:correct).id, commentable: q)
    assert_difference('Comment.count', -1) do
      q.destroy
    end
    
    q =  Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
    PgSearch::Multisearch.rebuild(Question)
    assert_difference('PgSearch::Document.count', -1) do
      q.destroy
      PgSearch::Multisearch.rebuild(Question)
    end
    
     file = fixture_file_upload("test_csv.csv", "text/csv")
    # set answers spreadsheet/update answer
     spread_sheet_question = Question.create(duplicate: nil, question_text: "a"*31, title: "b"*16, user_id: users(:correct).id, tag_list: "tag1")
     spread_sheet_question.update(spread_sheet_upload_attributes: {sheet: file})
    
    assert_difference('SpreadSheetUpload.count', -1) do
      spread_sheet_question.destroy
    end
  end
end
