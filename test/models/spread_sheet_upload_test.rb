require 'test_helper'

class SpreadSheetUploadTest < ActiveSupport::TestCase
  
  test "cannot save without attachable" do
    # set up file for attachment
    extend ActionDispatch::TestProcess
    file = fixture_file_upload("test_csv.csv", "text/csv")
    # set up count and empty var to compare and assign
    s = nil
    s_count = SpreadSheetUpload.count
    # settings in spreadsheet model will throw exception if attachable is empty but if that requirement is lost then we wont knowif the test is still valid.
    begin
    # should throw exception due to current spreadsheet model methods, but will try to create spreadsheet and assert it does not exist
      assert_no_difference('SpreadSheetUpload.count', "Spread sheet upload created without attachable") do
        s = SpreadSheetUpload.create(sheet: file)
      end
      assert(!s.errors.empty?)
    rescue
    # when error thrown just check that count as not changed
    assert_equal(s_count, SpreadSheetUpload.count, "spread sheet upload created without an attachable")
    end
  end
  
  test "that path is being set correctly for uploads" do
    # set up file for attachment
    extend ActionDispatch::TestProcess
    file = fixture_file_upload("test_csv.csv", "text/csv")
    sq = SpreadSheetUpload.create(sheet: file, attachable: questions(:correct))
    # Assert that the path is correct for a question
    assert_equal(sq.sheet.path, "question/#{questions(:correct).id}/test_csv.csv")
    # Assert that the path is correct for a answer
    sa = SpreadSheetUpload.create(sheet: file, attachable: answers(:correct))
    # Assert that the path is correct for a question
    assert_equal(sa.sheet.path, "answer/#{answers(:correct).id}/test_csv.csv")
  end
end
