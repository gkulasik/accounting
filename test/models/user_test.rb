require 'test_helper'

class UserTest < ActiveSupport::TestCase

# change_one_attribute is a helper method in test_helper for easy changing of one attribute about an object.

  def setup 
    # set up of regular user/question that will be used.
    @user = User.new(first_name: "greg", last_name: "kulasik", username: "pierce77127", email: "gkulasik@yahoo123.com", password: "testtest", password_confirmation: "testtest")
    @question = Question.new(title: "g"*16, question_text: "t"*31)
    @question.tag_list = "tag1"
  end
  
  test "save a correctly filled in user" do
    # make sure a valid user is saved
    user = users(:correct)
    assert user.save, "did not save a correct user"
  end
  
  test "do not save a blank user" do
    # make sure will not save a blank user
    user = User.new
    assert_not user.save, "saved a blank user"
  end
  
  test "do save a user without a first name" do
    # check validation that first name does not need to be filled in
    assert change_one_attribute(users(:correct), "first_name", nil).save, "did not save a user without a first name"
  end
  
  test "do save a user without a last name" do
    # check validation that last name does not need to be filled in
    assert change_one_attribute(users(:correct), "last_name", nil).save, "did not save a user without a last name"
  end
  
  test "do not save a user without a username" do
     # check validation that last name must be filled in
    assert_not change_one_attribute(users(:correct), "username", nil).save, "saved a user without a username"
  end
  
  test "do not save user without an email" do
    # check validation that user must have email
    assert_not change_one_attribute(users(:correct), "email", nil).save, "saved a user without an email"
  end
  
  test "can change profile information" do
    # check that can change profile info without issue
    assert change_one_attribute(users(:correct), "first_name", "brian").save, "did not allow changing of profile information"
  end
  
  test "should delete questions when user destroyed db level" do
    @user.save!
    #when user is deleted the users questions shoudl also be destroyed 
    @user.questions.create!(title: "g"*16, question_text: "t"*31, tag_list: "tag1, tag2")
    assert_difference('Question.count', -1) do
      @user.delete
    end
  end
  
  test "should delete answers when user destroyed db level" do
    @user.save!
    @question.user = @user
    @question.save!
    #when user is deleted the users answers shoudl also be destroyed 
    @user.answers.create!(answer_text: "text_", question: @question)
    assert_difference('Answer.count', -1) do
      @user.delete
    end
  end
  
  test "should delete comments when user destroyed db level" do
    @user.save!
    @question.user = @user
    @question.save!
    #when user is deleted the users comments should also be destroyed 
    @user.comments.create!(content: "this is content", commentable: @question)
    assert_difference('Comment.count', -1) do
      @user.delete
    end
  end
  
  test "should delete visits when user destroyed db level" do
    skip("Test when have visits implemented. Possibly at controller level?")
    @user.save!
    @user.visits.create!(content: "this is content", commentable: @question)
    assert_difference('Comment.count', -1) do
      @user.delete
    end
  end
  
  test "should return the output name of a user (first/last name) or the username" do
    # test that since pref_name not selected that output_name is username
    assert users(:correct).output_name == users(:correct).username, "user without set pref_name does not return the username"
    change_one_attribute(users(:correct), "pref_name", "real")
    # change pref name and then check that first/last name is returned
    assert users(:correct).output_name == "#{users(:correct).first_name} #{users(:correct).last_name}", "user with set pref_name does not return the first/last name"
    # change the first name to nil to test that both f and l name must be filled in to show f/l name as output name
    change_one_attribute(users(:correct), "first_name", nil)
    assert users(:correct).output_name == users(:correct).username, "user without first_name does not return username"
  end
  
  test "first_last blank method" do
    # both are filled in so shoudl return true
    assert !users(:correct).first_last_blank?, "method says that first and last name are blank, when they are not"
    # make both blank
    users(:correct).update_attributes(first_name: "", last_name: "")
    # should return false
    assert users(:correct).first_last_blank?, "method says that first and last name are not blank when they are"
    # fill in one
    users(:correct).update_attributes(first_name: "greg", last_name: "")
    # shoudl still return false
    assert users(:correct).first_last_blank?, "only first name filled in, both first and last name must be filled in"
    # fill in other
    users(:correct).update_attributes(first_name: "", last_name: "kulasik")
    # shoudl still return false
    assert users(:correct).first_last_blank?, "only last name filled in, both must be filled in"
  end
  
  test "test username/email case insensitivity" do
    @user_double = User.new(first_name: "greg", last_name: "kulasik", username: "PiErcE77127", email: "kulasik@yahoo123.com", password: "testtest", password_confirmation: "testtest")
    @user.save
    # test username case insensitivity
    assert_no_difference("User.count", "saved a user with same username (but difference cases)") do
      @user_double.save
    end
    # test email case insensitivity
    @user_double = User.new(first_name: "greg", last_name: "kulasik", username: "lalalaierce77127", email: "Gkulasik@yahoo123.com", password: "testtest", password_confirmation: "testtest")
    assert_no_difference("User.count", "saved a user with same email but different cases") do
      @user_double.save
    end
  end
  
  test "should filter out script tags from content" do
    # Make sure html is sanitized
    # save a user summary with html script tags
    @user.user_summary = "<p>this is a not text</p><script>window.alert('sometext')</script><p>this is more text</p>"
    @user.save
    w = @user
    # after save assert those tags are removed.
    assert_not w.user_summary.include?("<script>")
    assert_not w.user_summary.include?("</script>")
  end
  
  test "should not allow pref_name if first/last is empty" do
    user = users(:correct)
    # shoudl not allow user to select prefname of 'real' if first name  or last name is blank
    assert_not user.update(pref_name: "real", first_name: "")
    assert_not user.update(pref_name: "real", last_name: "")
    # shoudl allow user to select 'real' if both are not blank
    assert user.update(pref_name: "real", first_name: "hello", last_name: "hello2")
  end
end
