require 'test_helper'

class UserPolicyTest < ActiveSupport::TestCase
    test "user edit custom" do
        # Test custom edit policy for User
        assert_equal UserPolicy.new(users(:correct), users(:correct)).edit?, true #can edit own profile
        assert_equal UserPolicy.new(users(:admin), users(:correct)).edit?, true #admin can do anything
        assert_equal UserPolicy.new(users(:no_posts), users(:correct)).edit?, false #other user cannot edit a different persons profile
        assert_equal UserPolicy.new(nil, users(:correct)).edit?, false #not signed in user cannot edit others profile
    end
end