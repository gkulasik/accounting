require 'test_helper'

class ApplicationPolicyTest < ActiveSupport::TestCase
    setup do
        # Test default appication policies on each of the records that could be used 
        @models = [questions(:correct), answers(:correct), comments(:correct)]
        # Each test performs the same tests on each of the above types.
        
        # users(:correct) is owner of each of the above
        # users(:no_posts) has no posts so cannot be owner of any
        # users(:admin) is admin
        
    end
    
    test "index default" do
        @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).index?, true # index always returns true
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).index?, true #index always returns true
            assert_equal ApplicationPolicy.new(users(:admin), chosen).index?, true #index always returns true
            assert_equal ApplicationPolicy.new(nil, chosen).index?, true #index always returns true
         end
    end
    
    test "show default" do
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).show?, true # show will return true if record (chosen) exists.
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).show?, true # should return true since record (chosen) exists
            assert_equal ApplicationPolicy.new(users(:admin), chosen).show?, true # should return true since record (chosen) exists
            assert_equal ApplicationPolicy.new(nil, chosen).show?, true # should return true since record (chosen) exists
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen.class.new()).show?, false # does not exists yet (not saved) so cannot show 
         end
    end
    
    test "new default" do
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).new?, true #any signed in user can make new
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).new?, true # any signed in user can make new
            assert_equal ApplicationPolicy.new(nil, chosen).new?, true #default can make new if not signed in
            assert_equal ApplicationPolicy.new(users(:admin), chosen).new?, true #admin can do anything
         end
    end
    
    test "update default" do
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).update?, true # is owner of record == can update
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).update?, false # must be owner of record or admin
            assert_equal ApplicationPolicy.new(users(:admin), chosen).update?, true #admin can update
            assert_equal ApplicationPolicy.new(nil, chosen).update?, false # must be owner of record or admin 
         end
    end
    
    test "edit default" do
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).edit?, true # owner can edit
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).edit?, false # must be owner of record or admin
            assert_equal ApplicationPolicy.new(users(:admin), chosen).edit?, true #admin can edit
            assert_equal ApplicationPolicy.new(nil, chosen).edit?, false # must be owner of record or admin
        end

    end

    test "destroy default" do
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).destroy?, true # owner can destroy
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).destroy?, false # must be owner of record or admin
            assert_equal ApplicationPolicy.new(users(:admin), chosen).destroy?, true #admin can edit
            assert_equal ApplicationPolicy.new(nil, chosen).destroy?, false # must be owner of record or admin
         end

    end
    
    test "can_post default" do
        # chosen does not matter in this loop since just matters if signed in.
         @models.each do |chosen|
            assert_equal ApplicationPolicy.new(users(:correct), chosen).can_post?, true #signed in user can post
            assert_equal ApplicationPolicy.new(users(:no_posts), chosen).can_post?, true #signed in user can post
            assert_equal ApplicationPolicy.new(users(:admin), chosen).can_post?, true #admin can post
            assert_equal ApplicationPolicy.new(nil, chosen).can_post?, false #must be signed in to post
         end
    end
    
end