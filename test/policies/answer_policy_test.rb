require 'test_helper'

class AnswerPolicyTest < ActiveSupport::TestCase
    
    setup do
        # general use vars that are commonly used during tests
        @answer = answers(:no_comment)
        @a_owner = users(:correct)
        @no_posts = users(:no_posts)
        @admin = users(:admin)
    end
    
    test "answer destroy custom" do
        # Test custom destory policy for Answers
        assert_equal AnswerPolicy.new(@a_owner, @answer).destroy?, true # can delete cause user owns question
        assert_equal AnswerPolicy.new(@no_posts, @answer).destroy?, false # cant delete cause user has no posts (does no own question)
        assert_equal AnswerPolicy.new(@admin, @answer).destroy?, true # can delete cause admin can do anything
        # test policy with comments rules (cant delete if has comments)
        c = @answer.comments.build(content: "comment test", user_id: @a_owner.id) 
        c.save
        assert_equal AnswerPolicy.new(@a_owner, @answer).destroy?, false # answer owner cant delete if there are comments
        assert_equal AnswerPolicy.new(@admin, @answer).destroy?, true # admin can always delete
    end
    
    test "answer update custom" do
        # Test custom update policy for Answers
        assert_equal AnswerPolicy.new(@a_owner, @answer).update?, true # answer owner can update answer
        assert_equal AnswerPolicy.new(@no_posts, @answer).update?, false # non owner cannot update 
        assert_equal AnswerPolicy.new(@admin, @answer).update?, true # admin can update
        # test that quesiton owner cannot update the answer of other users
        q = Question.create(duplicate: nil, question_text: "323"*15, title: "123"*10, user: @no_posts, tag_list: "Tag")
        a = Answer.create(answer_text: "qwe"*15, question: q, user: @a_owner)
        assert_equal AnswerPolicy.new(@no_posts, a).update?, false #answer owner cannot update answers that are not theirs
    end
    
    test "answer update select correct answer" do
        # Test custom can_select_correct_answer policy for Answers. Used to see if X has permission to set answer as correct.
        assert_equal AnswerPolicy.new(@no_posts, @answer).can_select_correct_answer?, false # random person who does not own answer or quesiton cannot set answer as correct
        assert_equal AnswerPolicy.new(@admin, @answer).can_select_correct_answer?, true # admin can set answer as correct
        # Test that the quesiton owner can set an answer as correct
        q = Question.create(duplicate: nil, question_text: "323"*15, title: "123"*10, user: @no_posts, tag_list: "Tag")
        a = Answer.create(answer_text: "qwe"*15, question: q, user: @a_owner)
        assert_equal AnswerPolicy.new(@no_posts, a).can_select_correct_answer?, true # question owner can set answer as correct
    end
    
    test "answer second_post custom" do
        # Test custom second_post? policy for Answers
        assert_equal AnswerPolicy.new(nil, @answer).second_post?, false # is user is nil must be false (cant be second post)
        # create a new question and answer
        q = Question.create(duplicate: nil, question_text: "323"*15, title: "123"*10, user: @no_posts, tag_list: "Tag")
        a = Answer.create(answer_text: "qwe"*15, question: q, user: @no_posts)
        assert_equal AnswerPolicy.new(@no_posts, a).second_post?, false # only haveing one answer to edit (which is why Answer.id is not null) means it is not second post yet. 
        assert_equal AnswerPolicy.new(@no_posts, Answer.new(question: q)).second_post?, true # when its a new answer then should be seen as second post and return true
        # create another new answer to show that if you already have two answers or more if you are editing it will not give you 2nd post warnings
        a2 = Answer.create(answer_text: "qwe"*15, question: q, user: @no_posts) # make another answer 
        assert_equal AnswerPolicy.new(@no_posts, a2).second_post?, false # this should not set off the second post alert because Answer.id is set which means they are editing
                                                                         # We dont want the warning when editing just when creating second post
        #  Test that still bring up second_post true when more than one answer is created by user
        assert_equal AnswerPolicy.new(@no_posts, Answer.new(question: q)).second_post?, true # when its a new answer then should be seen as second post and return true
        assert_equal AnswerPolicy.new(@no_posts, Answer.new(question: questions(:correct))).second_post?, false # this is not a second post for the same question so no popup.
    end
end