require 'test_helper'

class QuestionPolicyTest < ActiveSupport::TestCase
    setup do
        # set up various users and test objects but not saved (will edit and save in tests)
       @q_owner = users(:no_posts)
       @user = users(:correct)
       @admin = users(:admin)
       @q = Question.new(duplicate: nil, question_text: "323"*15, title: "123"*10, user: @q_owner, tag_list: "Tag")
       @a = Answer.new(answer_text: "qwe"*15, user: @q_owner)
       @c = Comment.new(content: "234"*13, user: @user)   
    end
    
    test "question destroy custom" do
        # Test custom destory policy for question
        @q.save
        assert_equal QuestionPolicy.new(@q_owner, @q).destroy?, true # should be able to destroy an empty question
        # save the answer as belonging to the question
        @a.question = @q 
        @a.save
        assert_equal QuestionPolicy.new(@q_owner, @q).destroy?, false # should not be able to delete question since has an answer now
        # Delete the answer and assign the coment to the question
        @a.delete
        @c.commentable = @q
        @c.save
        assert_equal QuestionPolicy.new(@q_owner, @q).destroy?, false # the user should not be able to destroy it cause it has a comment
        # save the answer as belonging to the question (to test both answer an comment on question)
        @a = Answer.new(answer_text: "qwe"*15, user: @q_owner)
        @a.question = @q 
        @a.save
        assert_equal QuestionPolicy.new(@q_owner, @q).destroy?, false # the user should not be able to destroy it cause it has a comment and answer
        assert_equal QuestionPolicy.new(@admin, @q).destroy?, true # admin can do what ever they want
    end
    
    
    test "question index custom" do
      skip('Cannot access params')
      assert_equal QuestionPolicy.new(@admin, @q).index?, true # admin can see consumer version or admin version of index
      assert_equal QuestionPolicy.new(@user, @q).index?, false # user can only see public view (false)
      assert_equal QuestionPolicy.new(@admin, @q).index?, false #admin can see public view is selected
    end
end
