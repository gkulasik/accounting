require 'test_helper'
 class CommentsHelperTest < ActionView::TestCase
   include ApplicationHelper
  
  test "comment placeholder helper" do
    #   redefine curent user to be that no one is signed in.
    def current_user
        nil 
    end
    # check that since not signed in the helper will not allow to post
     assert_equal(t('general.comment.can_not_post'), comment_placeholder_helper(Comment.first)) 
    #  redefine back to being signed in. this also resets the definition back to what is expected by other tests
    def current_user
        users(:correct) 
    end
    # assert that can post now that signed in
     assert_equal(t('general.comment.can_post'), comment_placeholder_helper(Comment.first))
  end
end