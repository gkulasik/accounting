require 'test_helper'
 class TagsHelperTest < ActionView::TestCase
  test "tag cloud display helper test" do
    #   Get tag cloud
      results = Question.tag_cloud_popular(true)
        display = ""
        # create expected output
        tag_cloud(results, %w(css1 css2 css3 css4)) do |tag, css_class| 
            display += content_tag :span, class: 'label', style: "margin: 3px;" do
                         concat (link_to("#{tag.name} x #{tag.taggings_count}", questions_path(with_tags: tag.name), :class => css_class))
                    end
        end 
        # assert expected output matches actual 
      assert_equal(display.html_safe, tag_cloud_display_helper(results))
  end
end