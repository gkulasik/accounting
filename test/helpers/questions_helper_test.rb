require 'test_helper'
 class QuestionsHelperTest < ActionView::TestCase
  include ApplicationHelper
  setup do 
   # set up to give question a specific tag list
    @q = Question.first
    @q.tag_list = "Tag1, Tag2, Tag3"
    @q.save
  end
   
   test "question display poster info helper" do
    # create expected output using set up question @q
     expected_output = content_tag :div, class: "question_poster_info_#{@q.id}", style: "text-align: left;" do
      concat content_tag(:span, 
      "Answers: #{@q.answers.count}<br />
    Asked: #{formatted_created_at(@q)}  by #{link_to(@q.user.output_name, user_path(@q.user.username))}<br /><br />".html_safe)
    end
    # Once created then test output against expected.
     assert_equal(expected_output.html_safe, question_display_poster_info_helper(@q))
   end
   
   test "question tag display helper" do
    expected_output = ""
    # make sure that tag ehlper that displays tags in search and index display correctly
    # recreate output (expected output)
    @q.tag_list.each do |tag|
     expected_output += "&nbsp;"
     expected_output += content_tag :span, class: "label" do
        concat link_to(tag, questions_path(with_tags: tag))
      end
    end
    # put the expected output in a div (continueing creating expected output) hard to double nest concats
    expected_output = content_tag :div, class: 'question_results_tags' do
     concat expected_output.html_safe
    end
    # assert that are equal (expected == actual)
    assert_equal(expected_output.html_safe, question_tag_display_helper(@q))
   end
end