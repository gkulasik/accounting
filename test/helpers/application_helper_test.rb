require 'test_helper'
 class ApplicationHelperTest < ActionView::TestCase
  
   test "should return objects nicely formated created at string" do
    # test that output matchs helper output
     assert_equal users(:correct).created_at.strftime("%m/%d/%y %H:%M"), formatted_created_at(users(:correct)), 'formatted_created_at is not returning formatted string'
   end
   
   test "file upload form helper" do
    test_type = ""
    output = "<div class=\"input file optional %{test_type}_spread_sheet_upload_sheet\"><label class=\"file optional control-label\" for=\"%{test_type}_spread_sheet_upload_attributes_sheet\">Supplement Attachment</label><input class=\"file optional\" type=\"file\" name=\"%{test_type}[spread_sheet_upload_attributes][sheet]\" id=\"%{test_type}_spread_sheet_upload_attributes_sheet\" /></div>"
     # Test that the expected output for the file upload helper matchs that for a question or answer. 
     # Merged into 1 test by creating a template for file upload helper (output var) and then interpolating the 'type'
     # Put in form to provide the form instance for the file upload helper to work.
    form_for(Question.new) do |f|
     expected_output = output % {test_type: "question"} 
     # test for questions
     assert_equal(expected_output, file_upload_form_helper(f, f.object.build_spread_sheet_upload))
    end
    
    form_for(Answer.new) do |f|
     expected_output = output % {test_type: "answer"} 
     # test for answers
     assert_equal(expected_output, file_upload_form_helper(f, f.object.build_spread_sheet_upload))
    end
   end
   
   test "download file with icon helper" do
    # Assert created same output as expected
    assert_equal("<a href=\"test_file_url\">test_file_name <i class=\"fi-download\"></i></a>", download_file_with_icon_helper("test_file_name", "test_file_url"), "helper not giving out correct download file icon helper HTML")
  end
  
   test "button class helper" do
    # ake sure correct answer button class is provided by helper for each type of postable class
    objs = [Answer.new, Question.new, Comment.new]
    # For each type of postable
    objs.each do |a|
      # Set the current user
      def current_user
       users(:correct)
      end
       # test that the output of the helper is correct (enabled for signed in user) 
      assert_equal("", button_class_helper(a), "button should not be disabled")
      # set user to signed out
      def current_user
       nil
      end
      # test that the output of the helper is correct (disabld for not signed in user)
      assert_equal("disabled", button_class_helper(a), "button should be disabled")
    end
    # reset current_user cause in this scope it can affect other tests
    def current_user
       users(:correct)
    end
   end
   
   test "spread sheet upload download display helper" do
    # Create file
    file = fixture_file_upload("test_csv.csv", "text/csv")
    # create question with the file
    spread_sheet_question = Question.create(duplicate: nil, question_text: "b"*32, title: "ab"*15, user: users(:correct), tag_list: "tag1, tag2", spread_sheet_upload_attributes: {sheet: file})
    # create content to test against
    output = content_tag :strong do
     concat "View attached file: #{download_file_with_icon_helper(spread_sheet_question.spread_sheet_upload.sheet_file_name, spread_sheet_question.spread_sheet_upload.sheet.url)}".html_safe
    end
    # assert that the helper returns same output
    assert_equal(output, spread_sheet_upload_download_display_helper(spread_sheet_question))
   end
 end