require 'test_helper'
 class AnswersHelperTest < ActionView::TestCase
   include ApplicationHelper, SimpleForm::ActionViewExtensions::FormHelper
  # needed for application helper methods / needed to create form same way as in views. ^
  def setup
   # Set up current_user method to test
    def current_user
      users(:correct)
    end
  end
   
   test "answer display poster info helper" do
    answer = Answer.last
    # test returns content as expected
    # constructs duplicate of what is expected for a certian answer
     return_result = content_tag :div, class: "answer_results" do
      concat content_tag(:span, 
      "Answered: #{formatted_created_at(answer)} by #{link_to(answer.user.output_name, user_path(answer.user.username))}".html_safe)
    end
    # assert that this output is the same as what the helper ouputs. 
    assert_equal(return_result, answer_display_poster_info_helper(answer))
   end
   
   test "answer input text helper" do
    a = Answer.last
    # When user logged in (current_user set) there should be no output
    assert_equal("", answer_input_text_helper(a))
    # Reassign current_user
    def current_user
     nil
    end
    # When not logged in (current_user is not set) there should be a message 
    assert_equal("Must be signed in to post answers.", answer_input_text_helper(a))
   end
   
   test "answer text box helper" do
    a = Answer.new
    # When user logged in (current_user set) there should be certain output
    assert_equal("tinymce", answer_text_box_helper(a), "should allow to post")
     # Reassign current_user
    def current_user
     nil
    end
     # When not logged in (current_user is not set) there should be no output
    assert_equal("", answer_text_box_helper(a), "should NOT allow to post")
   end
   
   
   test "answer correct checkmark display helper" do
    # Get two answers, both belong to questions(:correct)
    a1 = answers(:correct)
    a2 = answers(:no_comment)
    
    # Make one of them the correct answer for the question
    q = questions(:correct)
    q.correct_answer = a1
    q.save
    
    # Duplicate output from helper (to test output)
    output = content_tag :h1 do 
     concat "<span data-tooltip aria-haspopup=\"true\" 
              class=\"has-tip\" title=\"#{t('general.answer.marked_correct')}\">
              <i class=\"fi-check\"></i></span>".html_safe
    end
    # Assert that for the correct answer the output matches but for the incorrect answer the text does not match.
    assert_equal(output, answer_correct_checkmark_display_helper(a1))
    assert(output != answer_correct_checkmark_display_helper(a2))
   end
 
   test "answer poster info question show" do
    answer = answers(:correct)
    # recreate the output of the helper
    output = content_tag :div, class: 'panel' do
      concat "Answered: #{answer.created_at.strftime("%m/%d/%y %H:%M")}
        <br />
        Answered by: #{link_to answer.user.output_name, user_path(answer.user.username)}".html_safe
        
    end
    # assert create output is same as helpers return value
    assert_equal(output, answer_poster_info_question_show(answer))
   end
 
   test "answer mark correct answer form helper" do
    # tests form for marking answer correct
    question = questions(:correct)
    answer = question.answers.first
    # recreate form for makr answer correct. 
    output = simple_form_for(answer) do |f|
          f.input :correct_answer_id, as: :hidden,  :input_html => { :value => question.id }
          link_to 'Mark answer as correct <i class="fi-check"></i>'.html_safe, "#", :onclick => "$('#edit_answer_#{answer.id}').submit()"
    end
    # Assert outputs are equal
    assert_equal(output, answer_mark_answer_correct_form_helper(answer, question))
   end
 end