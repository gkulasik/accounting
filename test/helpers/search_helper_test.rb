require 'test_helper'
 class SearchHelperTest < ActionView::TestCase
  include QuestionsHelper, ApplicationHelper, AnswersHelper # because test display extra helper used multiple other helpers within it (split logic to where it should be)
 
  setup do
   # Rebuild search indecies as test db seems to need it
    PgSearch::Multisearch.rebuild(Answer)
    PgSearch::Multisearch.rebuild(Question)
  end
   
   test "display title helper" do
    # multiple search queries to try and assert things about the results from test
    to_loop = ["hello hello question", "this is a test answer", "this is a test", "This is a test for a question"]
    to_loop.each do |search|
     # for each search do the search 
     search_obj = PgSearch.multisearch(search).first
     # get the first results
     result = search_obj.searchable
     # set the variables that are unique to the type of result (since output is almost identical)
    output = result.class.name + ": "
    if result.class.name == "Question"
     title = result.title
     text = result.question_text
     length = t('general.char_display.question')
    else
     title = result.question.title
     text = result.answer_text
     length = t('general.char_display.answer')
    end
    # create the expected output based on static and dynamic (jsut set vars ^) 
    output = "<strong>" + output + link_to(title, result).html_safe + "</strong>"
    output = output + "<br />" + truncate_html(text, length: length).html_safe
    # assert expected output is same as actual
    assert(output == display_title_helper(search_obj), output+ " vs. "+ display_title_helper(search_obj) + "should be the same")
   end
   end
   
   test "display extra helper" do
    # Get the a PGsearch doc for a question and answer
    q = Question.last.pg_search_document
    a = Answer.last.pg_search_document
    # test that correct output is given based on object passed in, (Has correct content)
    assert(display_extra_helper(q).include?("question_results_tags"))
    assert(display_extra_helper(a).include?(""))
   end
   
   test "display poster info helper" do
    # Get the a PGsearch doc for a question and answer
    q = Question.last.pg_search_document
    a = Answer.last.pg_search_document
    # Assert has some correct content to see that output is correct
    assert(display_poster_info_helper(q).include?("Asked:"))
    assert(display_poster_info_helper(a).include?("Answered:"))
   end
 end