class SearchRoutesTest < ActionController::TestCase

  test "should route to search results" do
    # create routes from scratch and then compare to see that routes to correct controller/action
    assert_routing '/results', { controller: "search", action: "results" }
  end
  
end