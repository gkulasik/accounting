class CommentRoutesTest < ActionController::TestCase

  test "should route to create comment" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Simulate POST
    assert_routing({ method: 'post', path: '/comments' }, { controller: "comments", action: "create" })
  end
  
  test "should route to destroy comment" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Simulate DELETE
    assert_routing({ method: 'delete', path: '/comments/1' }, { controller: "comments", action: "destroy", id:"1" })
  end
  
end