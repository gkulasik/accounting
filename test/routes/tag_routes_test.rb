class TagRoutesTest < ActionController::TestCase

  test "should route to tags" do
    # create routes from scratch and then compare to see that routes to correct controller/action
    assert_routing '/tags', { controller: "tags", action: "index" }
  end
  
end