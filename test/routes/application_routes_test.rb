class ApplicationRoutesTest < ActionController::TestCase

  test "should route to root path" do
    # Aesert that root_path is recognized and routes to correct place as set in routes.
    opts = {controller: 'questions', action: 'index'}
    assert_recognizes opts, '/' 
  end
  
  
end