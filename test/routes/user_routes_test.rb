class UserRoutesTest < ActionController::TestCase

  test "should route to user profile" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing '/profile/'+users(:correct).username, { controller: "users", action: "show", username: users(:correct).username }
  end
  
  
end