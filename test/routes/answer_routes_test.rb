class AnswerRoutesTest < ActionController::TestCase
  
  test "should route to answers show" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing '/answers/1', { controller: "answers", action: "show", id: "1" }
  end
  
  test "should route to create answer" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Need to simulate POST for create
    assert_routing({ method: 'post', path: '/answers' }, { controller: "answers", action: "create" })
  end
  
  test "should route to destroy answer" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Need to simulate DELETE for destroy
    assert_routing({ method: 'delete', path: '/answers/1' }, { controller: "answers", action: "destroy", id:"1" })
  end
  
  test "should route to update answer" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Test both ways to update - patch and put
    # Simulate both methods and make sure goes to same place
    assert_routing({ method: 'patch', path: '/answers/1' }, { controller: "answers", action: "update", id:"1" })
    assert_routing({ method: 'put', path: '/answers/1' }, { controller: "answers", action: "update", id:"1" })
  end
  
  test "should route to edit answer" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing('/answers/1/edit' , { controller: "answers", action: "edit", id:"1"})
  end

  test "should route to new answer (for fix on bad data)" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing( '/answers/new', { controller: "answers", action: "new"})
  end
end