class QuestionRoutesTest < ActionController::TestCase
  
  setup do
    # Set  up to have a quesiton ready for creating specific routes for testing
    @q = Question.first
  end
  test "should route to questions show" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing '/questions/'+@q.to_param.to_s, { controller: "questions", action: "show", id: @q.to_param.to_s }
  end
  
  test "should route to new question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing '/questions/new', { controller: "questions", action: "new" }
  end
  
  test "should route to index question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    assert_routing '/questions', { controller: "questions", action: "index" } 
  end
  
  test "should route to create question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Simulate POST
    assert_routing({ method: 'post', path: '/questions' }, { controller: "questions", action: "create"})
  end
  
  test "should route to edit question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
     assert_routing("/questions/#{@q.to_param.to_s}/edit", { controller: "questions", action: "edit", id: @q.to_param.to_s})  
  end
  
  test "should route to update question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Simulate/Test both PUT and PATCH
     assert_routing({ method: 'patch', path: '/questions/'+@q.to_param.to_s }, { controller: "questions", action: "update", id: @q.to_param.to_s}) 
     assert_routing({ method: 'put', path: '/questions/'+@q.to_param.to_s }, { controller: "questions", action: "update", id: @q.to_param.to_s}) 

  end
  
  test "should route to destroy question" do
    # create route from scratch (how it should look) and then assert it goes to a controller/action
    # Simulate DELETE
      assert_routing({ method: 'delete', path: '/questions/'+@q.to_param.to_s }, { controller: "questions", action: "destroy", id: @q.to_param.to_s}) 
  end
end