source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.17.1'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 4.0.2'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 2.5.3'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc


# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '~> 3.5.1'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '~> 1.2.0'
  
  # Suppresses the asset calls in the logs, this makes reading the logs easier (do not have to scroll through alot of asset calls)
  gem 'quiet_assets', '~> 1.1.0'
  
  # Both are used to provide usable error pages to help with diagnosis of errors
  gem 'better_errors', '~> 2.1.0'
  gem 'binding_of_caller', '~> 0.7.2'
  
  # This gem is used to ensure that referential integrity is maintained. It helps to generate the migrations for foreign keys. Make sure not to add ahoy tables to to users.
  gem 'immigrant', '~> 0.2.0'  # use 'rails generate immigration AddKeys' to have the system generate needed foreign keys
  
  # This gem speeds up the application by detecting N+1 queries. 
  gem 'bullet', '~> 4.14.4'
  
  # Gem to review code and find unused/dead routes
  # https://github.com/amatsuda/traceroute
  gem 'traceroute', '~> 0.5.0'
  
  # Static code vulnerability analysis
  # https://github.com/presidentbeef/brakeman
  gem 'brakeman','~> 3.0.5', require: false
  
  # Static code analyzer for code correctness/ruby guidelines
  # https://github.com/bbatsov/rubocop#cops
  gem 'rubocop', '~> 0.32.1', require: false
  
  # Another static code analyser for correctness.
  # https://github.com/whitesmith/rubycritic
  gem 'rubycritic', '~> 1.4.0', require: false
  
  # Provides performance info while in development to quash slow actions
  # https://github.com/MiniProfiler/rack-mini-profiler
  gem 'rack-mini-profiler', '~> 0.9.6'
  
  # Critiques code against Rails Best Practices
  # https://github.com/railsbp/rails_best_practices
  gem 'rails_best_practices', '~> 1.15.7'
end

group :production do
  # Gem for Heroku to run rails apps effectively
  gem 'rails_12factor'
end

# User gems
gem 'devise', '~> 3.4.1' # authentication
gem 'pundit', '~> 0.3.0' # authorization
gem 'ahoy_matey', '~> 1.1.0' # tracking/statistics  // Cannot test - may aim to replace eventually with testable event framework
gem 'sparkpost_rails', '~> 1.4.0' # mailing


# Frontend gems
gem 'foundation-rails', '~> 5.5.0.0' # visual front end
gem 'simple_form', '~> 3.1.0' # simple form builder
gem 'foundation-icons-sass-rails', '~> 3.0.0' # icons for display
gem 'tinymce-rails', '~> 4.2.8' # WYSIWIG editor
gem "auto_html", '~> 1.6.4' # change text to HTML automagically - only makes a link linked
gem 'sanitize', '~> 3.1.0' # sanitizes input, removes script tags
gem "nokogiri", '~> 1.6.5' 
gem "htmlentities", '~> 4.3.4'
gem 'truncate_html', '~> 0.9.3' # shortens HTML but respects tags

# File manipulation gems
gem "paperclip", '~> 4.2.1' # file uploads
gem 'aws-sdk', '~> 1.5.8' # storage


# General gems
gem "figaro", '~> 1.0.0' # used for safe access to env variables and secure upload to heroku
gem 'faker', '~> 1.4.3' # This gem is used to generate fake data for the site

# Search gems
gem 'pg_search', '~> 1.0.0' # searching 
gem 'kaminari', '~> 0.16.1' # pagination 
gem 'acts-as-taggable-on', '~> 3.5.0' # tags

# Backend gems
gem "resque", '~> 1.25.2' # for sepereate job processing
gem 'unicorn' # server to run multiple instances of app for better response and usablity.

# Explicit mention of Ruby version to use
ruby "2.1.5"