# Accounting Studies

This site will provide accounting students and professionals alike with a forum that permits the spread of accounting knowledge and benefits the entire accounting community. It is written in Ruby on Rails.

Find the live site at [www.accountingstudies.net](http://www.accountingstudies.net).

### Main idea: 
- **Free** - Anyone can access the site and view questions/answers so they can benefit from the community's knowledge
- **Extensive** - Not only can users ask questions, they can also upload files and sign in with their other accounts

### Platform:
- Rails: 4.2.0
- DB: Postgresql
- Testing: TestUnit
- Startup: rails s -b 0.0.0.0 (Nitrous.io)