class StagingEmailInterceptor
  def self.delivering_email(message)
    message.subject = "#{message.to.first}: #{message.subject}"
    message.to = [ENV['REDIRECT_EMAIL']]
  end
end