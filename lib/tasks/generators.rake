namespace :generators do
    
  desc "Create all test data at once"
  task all: :environment do
    user_generator
    question_generator
    answer_generator
    comment_generator
  end
  
  desc "Create questions for test data"
  task questions: :environment do
    question_generator
  end

  desc "Create answers as test data"
  task answers: :environment do
    answer_generator
  end

  desc "Create users as test data"
  task users: :environment do
    user_generator
  end

  desc "Create comments as test data"
  task comments: :environment do
    comment_generator
  end
  
  desc "Create tags as test data for questions"
  task tags: :environment do
     tag_generator
      
  end
  
#   Helper methods which allow the seperation and combining of the various tasks 
def question_generator


   if User.count > 5
     randGen = Random.new
     count = 0
     50.times do 
      user =  User.offset(rand(User.count)).first
      title = Faker::Lorem.sentence(5, false, 7)
      question_text = Faker::Lorem.paragraph(3, false, 6)
      question_text = "<p>"+question_text+"</p>"
      tags = Faker::Lorem.words(randGen.rand(1..5))
      Question.create(title: title, user: user, question_text: question_text, tag_list: tags)
      count += 1
      puts "Created Question : #{count}"
     end
    else
    puts "User count must be higher then 5"
   end
end

def answer_generator
    if Question.count > 5
      count = 0
      Question.all.each do |q|
        rand(0..4).times do
      user =  User.offset(rand(User.count)).first
      question = q
      answer_text = Faker::Lorem.paragraph(3, false, 6)
      Answer.create(answer_text: answer_text, user: user, question: question)
          count += 1
          puts "created answer : #{count}"
          # if q.answers.count >= 4 # to set some answers as correct. 
          #   q.correct_answer = q.answers.last
          # end
        end
      end
    else
    puts "Question count must be higher then 5"
    end
end

def user_generator
   count = 0
     50.times do 
       email = Faker::Internet.safe_email
       password = "password123"
       password_confirmation = password
       first_name = Faker::Name.first_name
       last_name = Faker::Name.last_name
       username = Faker::Internet.user_name(nil, %w(_))
       user_summary = Faker::Lorem.paragraph(3, false, 3)
       pref_name = rand(0..1) == 1? "username" : "real"
       User.create(email: email, password: password, password_confirmation: password_confirmation, first_name: first_name, last_name: last_name, username: username, user_summary: user_summary, pref_name: pref_name)
       count += 1
       puts "created user : #{count}"
    end
end
  
  def comment_generator
    if User.count > 5 && Answer.count > 5 && Question.count > 5
    count = 0
    2.times do # questions and answers
     40.times do
      user = User.offset(rand(User.count)).first
      content = Faker::Lorem.sentence
      commentable = rand(0..1) == 1 ? Question.offset(rand(Question.count)).first : Answer.offset(rand(Answer.count)).first
      Comment.create(user: user, content: content, commentable: commentable)
      count += 1
      puts "created comment : #{count}"
     end
    end
    else
  puts "Not enough commentable or users"
    end
  end
  
  
  def tag_generator
      randGen = Random.new
    Question.all.each do |q|
       q.tag_list = Faker::Lorem.words(randGen.rand(1..5))
       q.save
       puts "Added tag list: "+q.tag_list.to_s + " to question: " + q.id.to_s
    end
  end
end
