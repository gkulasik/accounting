class BooleanEnvWrapper < SimpleDelegator
  def fetch(key)
    val = super
    return true if val.to_s.downcase == 'true'
    return false if val.to_s.downcase == 'false'
    return val
  end
end