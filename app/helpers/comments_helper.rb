module CommentsHelper
    # Helper that will provide the correct text to show for whether a comment can be displayed or not. 
    def comment_placeholder_helper(comment)
       if policy(comment).can_post?
            t('general.comment.can_post')
       else
            t('general.comment.can_not_post')
       end
    end
    
end
