module TagsHelper
    include ActsAsTaggableOn::TagsHelper
    
    # Helper that displays a tag cloud. Uses CSS to style text size.
    # Links to filter of questions by tag.
    def tag_cloud_display_helper(tags)
        display = ""
        tag_cloud(tags, %w(css1 css2 css3 css4)) do |tag, css_class| 
            display += content_tag :span, class: t('display.styles.tag_label_color'), style: "margin: 3px;" do
                         concat (link_to("#{tag.name} x #{tag.taggings_count}", questions_path(with_tags: tag.name), :class => css_class))
                       end
        end 
        display.html_safe
    end
end
