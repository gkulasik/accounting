module MailerHelper
    include SparkpostHelper
    
    def full_email_send(user_id, subject, custom_merge_vars, template_name)
        merge_vars_extra = {
                "SUBJECT" => subject,
            }
        user = User.find(user_id)
        complete_merge_vars = mergeVars(user, custom_merge_vars.merge(merge_vars_extra))
        full_sparkpost_email_send(user.email, subject, complete_merge_vars, templateMapper(template_name)) 
    end
    
    def mergeVars(user, custom_merge_vars) # mapping of names to values
        default_merge_vars(user).merge(custom_merge_vars) 
    end
    
    def default_merge_vars(user) 
        {
          "OUTPUT_NAME" => user.output_name,
          "LOGIN_URL" => new_user_session_url,
          "ROOT_URL" => root_url,
          "SITE_NAME" => I18n.t('site_name'),
        }
    end 
    
    # see if needed
    def templateMapper(systemName) # string
        systemName.gsub(/[_]/, '-')  # Replace all _ with - (since that is what Sparkpost uses)
    end
    
        
end