module SparkpostHelper
    
        def full_sparkpost_email_send(user_email, subject, custom_merge_vars, template_id)
            transformed_merge_vars = vars_converter(custom_merge_vars, template_id)
            send_mail(user_email, subject, transformed_merge_vars)
        end
        
        def send_mail(email_address, this_subject, data)
            mail(to: email_address, subject: this_subject)
            # Add param sparkpost_data: data to supplement data to SparkPost 
        end

        def vars_converter(merge_vars, template_id) # hash
            returnable = {}
            merge_vars.each {|key, value| returnable[key.parameterize.underscore.to_sym] = value}
            
            {
                substitution_data: returnable,
                template_id: template_id
            }
        end
    
end