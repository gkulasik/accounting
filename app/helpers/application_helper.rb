module ApplicationHelper
  
  # Helper to procude nice output for file download link with icon.
  # Output produced, link to: file_name.docx (download icon)
  def download_file_with_icon_helper(file_name, file_url)
      tag = file_name+" "+ "<i class=\"fi-download\"></i>"
      tag = tag.html_safe
      return link_to(tag, file_url)
  end
  
  # Helper to create the upload field for files. 
  # Meant for spread_sheet_uploads, checks policy to verify that file can be uploaded.
  def file_upload_form_helper(builder, file)
    return builder.simple_fields_for :spread_sheet_upload, file do |su|
        su.input :sheet, required: false, label: "Supplement Attachment", disabled: !policy(builder.object).can_post?, as: :file
    end
  end
  
  # Helper to consistently format datetime.
  def formatted_created_at(object)
    return object.created_at.strftime("%m/%d/%y %H:%M")
  end
  
  # Helper that determines if the button will be enabled or disabled for posting.
  # Note: Button is still clickable. It is more of a style thing. Will redirect to sign in page if not logged in.
  def button_class_helper(to_authorize)
    if policy(to_authorize).can_post?
      ""
    else
      "disabled"
    end
  end
  
  # Helper to display a download link for a file with icon.
  def spread_sheet_upload_download_display_helper(postable)
    output = ""
    if !postable.spread_sheet_upload.nil?
      output = content_tag :strong do 
        concat "View attached file: #{download_file_with_icon_helper(postable.spread_sheet_upload.sheet_file_name, postable.spread_sheet_upload.sheet.url)}".html_safe
      end
    end
    return output.html_safe
  end
  
  # Helper to convert Devise and other framework flash messages into Foundation alert box classes.
  def flash_class(level)
    case level.to_sym
      when :notice then "alert-box success" # Devise
      when :success then "alert-box success"
      when :info then "alert-box info"
      when :error then "alert-box alert"
      when :alert then "alert-box alert"
    end
  end
end
