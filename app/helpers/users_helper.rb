module UsersHelper
    
    # Helper that displays avatar and links for delete/add.
    def profile_avatar_helper(user, form)
        if !user.nil? && !form.nil? && user.class == User
           return_string = ""
           label_string = "Profile Image"
          delete_label = "Delete Profile Image?"
            if !user.avatar_file_name.nil?
               return_string = image_tag(user.avatar.url(:thumb), alt: "Current Profile Image")
               return_string += form.input :delete_image, label: delete_label, as: :boolean 
               label_string = "Update Profile Image"
            end
           return_string += form.input :avatar, required: false, label: label_string, as: :file
        end
           return_string.html_safe
    end
    
    # Helper that displays the users avatar on profile page.
    # If there is no profile page then image displayed would be generic image.
    def avatar_display_helper
      #TODO remove instance variable and replace with parameter
        if !@user.avatar_file_name.nil?
             image_tag(@user.avatar.url(:small), alt: "#{@user.output_name} profile image") 
        else
            # If no default profile image then provide default. (From out site so more secure)
           image_tag('profile_placeholder.png', alt: "No profile image selected.", size: "140x150")
        end
    end
end
