module QuestionsHelper
  
  # Helper that displays question tags in index and search results.
  # Each tag wrapped in a label, seperated by &nbsp; due to content tag formatting.
  # Each tag links to a filter of questions by that tag.
  # Labled wrapped in div.
  def question_tag_display_helper(question)
  expected_output_partial = ""
    question.tag_list.each do |tag|
    expected_output_partial += "&nbsp;"
    expected_output_partial += content_tag :span, class: "label" do
        link_to(tag, questions_path(with_tags: tag))
      end
    end
    
    question_results_tags = content_tag :div, class: 'question_results_tags' do
      concat expected_output_partial.html_safe
    end
    question_results_tags.html_safe
  end
  
  # Helper that displays the question post and poster info.
  # Displays number of answers and who asked/when. 
  # Ex. output in div: Answers: x
  #                    Asked: (time) by (username)
  def question_display_poster_info_helper(question)
    return_result = content_tag :div, class: "question_poster_info_#{question.id}", style: "text-align: left;" do
      concat content_tag(:span, 
      "Answers: #{question.answers.count}<br />
    Asked: #{formatted_created_at(question)}  by #{link_to(question.user.output_name, user_path(question.user.username))}<br /><br />".html_safe)
    end
    return return_result.html_safe
  end
  
end
