module AnswersHelper
   
  # Helper to display the post and posters info:
  # Returns HTML string like this wrapped in a div: Answered: May 5 2015 by UserName
  def answer_display_poster_info_helper(answer)
    return_result = content_tag :div, class: "answer_results" do
      concat content_tag(:span, 
      "Answered: #{formatted_created_at(answer)} by #{link_to(answer.user.output_name, user_path(answer.user.username))}".html_safe)
    end
    return return_result.html_safe
  end
  
  # Helper to provide output for if can post an answer. Sets placeholder.
  def answer_input_text_helper(answer)
    if policy(answer).can_post?
      ""
    else
      "Must be signed in to post answers."
    end
  end
  
  # Helper that determines if tinymce loads.
  def answer_text_box_helper(answer)
    if policy(answer).can_post?   
      "tinymce"
    else
      ""
    end
  end
  
  # Helper that determines if it should display a checkmark to signify that the answer is marked as correct.
  def answer_correct_checkmark_display_helper(answer)
    output = ""
    if !answer.correct_answer_id.nil?
      output = content_tag :h1 do 
              concat "<span data-tooltip aria-haspopup=\"true\" 
              class=\"has-tip\" title=\"#{t('general.answer.marked_correct')}\">
              <i class=\"fi-check\"></i></span>".html_safe
      end
    end
    return output.html_safe
  end
 
  # Helper that displays the answer posters information on the question 'show' page.
  # TODO add row for 'Updated: {date}' to show last update
  def answer_poster_info_question_show(answer)
    output = content_tag :div, class: "panel" do
        concat "Answered: #{answer.created_at.strftime("%m/%d/%y %H:%M")}
        <br />
        Answered by: #{link_to answer.user.output_name, user_path(answer.user.username)}".html_safe
    end
    output.html_safe
  end
  
  # Helper that will create the form that a user can use to mark an answer correct.
  def answer_mark_answer_correct_form_helper(answer, question)
    # previous
    #     <% if answer.correct_answer_id.nil? && policy(answer).can_select_correct_answer? %>
    #   <%= simple_form_for(answer) do |f|%>
    #   <%= f.input :correct_answer_id, as: :hidden,  :input_html => { :value => @question.id } %>
    #   <%= link_to 'Mark answer as correct <i class="fi-check"></i>'.html_safe, "#", :onclick => "$('#edit_answer_#{answer.id}').submit()" %>
    #   <% end %>
    # <% end %>
    output = ""
    if answer.correct_answer_id.nil? && policy(answer).can_select_correct_answer?
      output = simple_form_for(answer) do |f|
          f.input :correct_answer_id, as: :hidden,  :input_html => { :value => question.id }
          link_to 'Mark answer as correct <i class="fi-check"></i>'.html_safe, "#", :onclick => "$('#edit_answer_#{answer.id}').submit()"
      end
    end
  end
end
