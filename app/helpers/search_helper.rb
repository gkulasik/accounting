module SearchHelper
  
  # Helper that displays search results titles.
  # Ex. output: Question: this is a question title
  #             <p>Truncated HTML of question text</p>
  # Grabs length for HTML from en.yml.
  def display_title_helper(searchObj)
    result = searchObj.searchable_type + ": "
    if searchObj.searchable_type == "Question"
      title = searchObj.searchable.title
      text = searchObj.searchable.question_text
      length = t('general.char_display.question')
    else
      title = searchObj.searchable.question.title
      text = searchObj.searchable.answer_text
      length = t('general.char_display.answer')
    end
    result = "<strong>" + result + link_to(title, searchObj.searchable).html_safe + "</strong>"
    result = result + "<br />" + truncate_html(text, length: length).html_safe
    return result.html_safe
  end
  
  # Helper that displays 'extra' info for search results.
  # At the moment this helper delegates the display of tags to the question helper.
  # No output for answers.
  def display_extra_helper(searchObj)
    #TODO  make display more data, make third helper for the side of the result
    case searchObj.searchable_type
      when "Question"
        result = question_tag_display_helper(searchObj.searchable)
      when "Answer"
        result = ""
      else
      result =  ""
    end
    return result.html_safe
  end
  
  # Helper that displays postre info.
  # Method delegates to the respective helpers for each model and then returns result.
  def display_poster_info_helper(searchObj)
    case searchObj.searchable_type
      when "Question"
        result = question_display_poster_info_helper(searchObj.searchable)
      when "Answer"
        result = answer_display_poster_info_helper(searchObj.searchable)
      else
      result =  ""
    end
    return result.html_safe
  end
end
