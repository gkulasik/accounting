class UserMailer < ApplicationMailer
    include MailerHelper
    
    
    # on reconfirmation success - intro to site email
    def welcome_email(user_id)
        
        subject = "Welcome to "+ I18n.t('site_name')+"!"
        merge_vars_welcome = {
          "TUTORIAL_URL" => "PUT URL IN"
        }
        @user = User.find_by(id: user_id)
        @login_url = new_user_session_url
        @tutorial_url = ""
        full_email_send(user_id, subject, merge_vars_welcome,"welcome_new_user")
    end
end
