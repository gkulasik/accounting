class PostableMailer < ApplicationMailer
    include MailerHelper
    # Get email to post owner when a comment made on it 
    # (comment to answer will get email to answer owner, comment on question will get email to question owner)
    def new_comment_on_post_email(postable, comment)
        user = postable.user
        subject = I18n.t('site_name')+": New comment on #{postable.class.to_s.downcase}"
        merge_vars_new_comment = {
          "NEW_POST_URL" => if postable.class.to_s == "Question"
                              postable 
                           else 
                              question_url(postable.question, anchor: "answer-#{postable.id}")
                           end,
          "POSTABLE_TYPE" => postable.class.to_s.downcase
        }
        @user = user
        @post = postable 
        @link = merge_vars_new_comment["NEW_POST_URL"]
        full_email_send(user.id, subject, merge_vars_new_comment,"new_post_on_post")
    end
    
    #   get email to question owner when a new answer posted
    def new_answer_on_post_email(question, answer)
        user = question.user
        subject = I18n.t('site_name')+": New answer on question"
        merge_vars_new_answer = {
          "NEW_POST_URL" => question,
          "POSTABLE_TYPE" => "question"
        }
        @user = user
        @question = question
        full_email_send(user.id, subject, merge_vars_new_answer,"new_post_on_post")
    end
end
