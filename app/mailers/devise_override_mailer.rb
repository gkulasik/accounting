class DeviseOverrideMailer < Devise::Mailer   
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  include MailerHelper
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views
  default from: ENV['DEFAULT_ROBOT_EMAIL']
  
    def confirmation_instructions(record, token, opts={})
        user = record
        subject = I18n.t('devise.mailer.confirmation_instructions.subject')
        
        merge_vars_confirm_account = {
          "CONFIRM_ACCOUNT_URL" => confirmation_url(record, confirmation_token: token)
        }
        @email = user.email
        @resource = user
        @token = token
        full_email_send(user.id, subject, merge_vars_confirm_account, "devise_confirm_account")
    #   super
    #   @token = token
    #   devise_mail(record, :confirmation_instructions, opts)
    end

    def reset_password_instructions(record, token, opts={})
        user = record
        subject = I18n.t('devise.mailer.reset_password_instructions.subject')
        
        merge_vars_reset_password = {
          "RESET_PASSWORD_URL" => edit_password_url(user, reset_password_token: token)
        }
        @resource = user
        @token = token
        full_email_send(user.id, subject, merge_vars_reset_password, "devise_reset_password")
        
    #   super
    #   @token = token
    #   devise_mail(record, :reset_password_instructions, opts)
    end

    def unlock_instructions(record, token, opts={})
        raise "This feature is not implemented. This should not be called. Record: "+ record + " token: "+ token
        super
    #   @token = token
    #   devise_mail(record, :unlock_instructions, opts)
    end

    def password_change(record, opts={})
        user = record
        subject = I18n.t('devise.mailer.password_change.subject')
        
        merge_vars_password_change = {}
        @resource = user
        full_email_send(user.id, subject, merge_vars_password_change, "devise_password_change")
    #   super
    #   devise_mail(record, :password_change, opts)
    end
end