class ApplicationMailer < ActionMailer::Base
  default from: ENV['DEFAULT_ROBOT_EMAIL']
  layout 'mailer'
end
