class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: [:index, :show]
  respond_to :html
  
  # Shows a list of questions.
  # Goes through list of params (below) and applys filters (if true).
  # Once the filters are applied (if any) then the results are paginated and returned.
  def index
    @questions = Question.where(nil)
    filtering_params(params).each do |key, value|
      @questions = @questions.public_send(key, value) if value.present?
    end
    @questions = @questions.page(params[:page])
    respond_with(@questions)
  end
  
  # Shows a question, its answers, and comments for both.
  # Obtains the question's answers and comments. 
  # 
  def show
    # id IS NOT NULL used because of the 'new' answer and comments which are created off of the question. 
    # This means they are included even if they are not saved yet. We don't want null ids.
    @answers = @question.answers.includes(:spread_sheet_upload, :comments).where("id IS NOT NULL").order("correct_answer_id ASC, created_at DESC")
    @comments_q = @question.comments.where("id IS NOT NULL") 
    
    # Create a new comment and answer for the question.
    @comment = @question.comments.build 
    @answer = @question.answers.build
    # record visit to question unless question owner
    if(current_visit != nil && current_user != @question.user && current_visit.ahoy_events.where(name: "private_view_question", item_id: @question.id).empty?)
      ahoy.track "private_view_question", id: @question.id
    end
    respond_with(@question)
  end

  # Makes a new question for the new form.
  def new
    @question = Question.new
    respond_with(@question)
  end

  # Provides a question to be edited.
  # Authorizes user to see if can edit question.
  def edit
    authorize(@question)
  end

  # Creates a question.
  # Sets the user based on who is logged in.
  # If success sets the flash message else will automatically display cause of simple form.
  def create
    @question = Question.new(question_params)
    @question.user = current_user
    if @question.save
    flash[:success] = t('messages.models.general.post', model: "Question")
    ahoy.track "public_created_question", id: @question.id
    end
    respond_with(@question)
  end

  # Updates a question.
  # Authorizes to see if user is authorized to update a question.
  # Updates question and sets flash message.
  def update
    authorize(@question)
    if @question.update(question_params)
      flash[:success] = t('messages.models.general.update', model: "Question")
      ahoy.track "public_updated_question", id: @question.id
    end
    respond_with(@question)
  end
  
  # Destroys a question.
  # Authorizes user if can destroy question.
  # Destroys question and sets flash message.
  def destroy
    authorize(@question)
    qId = @question.id
    @question.destroy
    flash[:success] = t('messages.models.general.delete', model: "Question")
    ahoy.track "public_deleted_question", id: qId
    respond_with(@question)
  end

  
  private
    # Set @question for controller methods.
    def set_question
      @question = Question.find(params[:id])
    end

    # Sets which params are permitted to enter the controller. Security.
    def question_params
      params.require(:question).permit(:title, :question_text, :duplicate, :answered, :user_id, :tag_list, :spread_sheet_upload_attributes => [:id, :sheet, :_destroy])
    end
    
    # Provides list of params to filter questions by. Linked to question model scopes.
    def filtering_params(params)
      params.slice(:with_tags, :unanswered, :recently_updated, :newest)
    end
end
