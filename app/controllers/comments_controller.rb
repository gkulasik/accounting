class CommentsController < ApplicationController
  before_action :set_comment, only: [:destroy]
  before_action :set_return_object, except: [:create]
  
  # Create a comment.
  # Create a comment off of the signed in user. This provides protection that a user must be signed in to create a comment.
  # This had to be done to accomodate the set_comment method (which would have interfered with the line above this one.)
  # set_return_object will set the page to return too after create.
  # Creates comment and assigns the flash message.
  # Will send post owner email that a comment has been posted.
  def create
    @comment = current_user.comments.build(comment_params)
    # This had to be done to accomodate the set_comment method (which would have interfered with the line above this one.)
    # The before_action expects @comment to be set before create is run.
    set_return_object  
    if @comment.save
      flash[:success] = t('messages.models.general.post', model: "Comment")
      ahoy.track "public_created_comment", id: @comment.id
      
      text = PostableMailer.new_comment_on_post_email(@comment.commentable, @comment).deliver_later!
      logger.info text
      redirect_to question_path(@go_to)
    else
      if !@comment.content.blank?
        flash[:alert] = t('messages.general_error')
      else
        flash[:alert] = t('messages.models.comment.blank')
      end
      redirect_to question_path(@go_to)
    end
  end

  # Destroys a comment.
  # Authorizes to make sure can delete the comment.
  # Deletes, sets flash message and redirects to the comments old post parent.
  def destroy
    authorize(@comment)
    cId = @comment.id
    @comment.destroy
    ahoy.track "public_deleted_comment", id: cId
    flash[:success] = t('messages.models.general.delete', model: "Comment")
    redirect_to question_path(@go_to)
  end


  
  private
  
  #  Sets the comment for usage in other methods.
  def set_comment
    @comment = Comment.find(params[:id])
  end
  
  # Gets the comments parent and returns that.
  def set_return_object
    if @comment.commentable_type == "Answer"
        @go_to = @comment.commentable.question
    else
        @go_to = @comment.commentable
    end
  end
  
  # Permitted params through form.
  def comment_params
    params.require(:comment).permit(:content, :commentable_id, :commentable_type, :user_id)
  end
end
