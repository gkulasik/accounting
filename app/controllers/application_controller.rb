class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit # For authorization
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized # rescue from forbidden actions
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_friendly_forward
  before_action :authenticate_user!
  before_action :display_flash
  
  
  def display_flash
    if params.has_key?(:flash)
      flash[:info] = params[:flash]
    end
  end
  
  # Path for after sign in is to profile page, also sets the session variable for menu bar.
  def after_sign_in_path_for(resource_or_scope) 
    session["output_name"] = current_user.output_name
    session["friendly_forward"] || user_path(resource_or_scope.username) 
  end
  
  # Path for after sign up is profile page.
  def after_sign_up_path_for(resource) 
    user_path(resource.username)
  end
  
  protected
  # This method captures the url a user is trying to access. 
  # If they are redirected to sign in due to it being protected 
  # then it will store the url in the session to be retrieved later by devise for a friendly forward.
  def set_friendly_forward 
  unless devise_controller? || request.xhr? || !request.get?
    session["friendly_forward"] = request.url
  end
  end
  
  # General method to be called by Pundit when a user is not authorized to do something. 
  # No customization at the moment, may add that in the future.
  # https://github.com/elabs/pundit#creating-custom-error-messages
  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end
  
  # Cofigures permitted parameters for Devise (what can go through the signup/login forms/update).
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password, :first_name, :last_name, :user_summary, :disable, :pref_name, :avatar, :delete_image) }
  end
end
