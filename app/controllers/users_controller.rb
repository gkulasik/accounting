class UsersController < ApplicationController
  before_action :set_user, only: [:show]
  skip_before_action :authenticate_user!, only: [:show]
  respond_to :html
  
  # Displays user profile page.
  def show
    ahoy.track "public_view_profile", id: @user.id
    respond_with @user
  end


  private
  
  # Sets user and includes their questions and answers. Use username to find users.
  def set_user
    @user = User.includes(:questions, :answers).find_by_username(params[:username])
  end

  
end
