class TagsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  
  # Displays page of tags.
  # Filters tag results by each param that is provided or if none then by most popular is default.
  # Paginate with custom value.
  def index
    @tags = Question.tag_cloud_popular(true)
    filtering_params(params).each do |key, value|
      @tags = Question.public_send(key, value) if value.present?
    end
    @tags = @tags.page(params[:page]).per(t('general.tags.index_per_page'))
  end
  
private 
  # The types of filters that are allowed for filtering tags. Related to question model (since that is what tags are on).
  def filtering_params(params)
      params.slice(:tag_cloud_popular, :tag_cloud_newest)
  end
end
