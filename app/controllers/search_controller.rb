class SearchController < ApplicationController
  skip_before_action :authenticate_user!, only: [:results]
  respond_to :html
  
  # Displays search results.
  # Gets search results based on search query and paginates.
  # If no results, redirect to root path with flash message about no results else display.
  def results
    @results = PgSearch.multisearch(params[:search_query]).preload(:searchable).page(params[:page])
    if @results.empty?
      # Use controller to create form and use methods to create search and then display results. Keep sepereate from question display/ integrate into DRY 
      flash[:alert] = "No results."
      redirect_to questions_path
    else
      render :results
    end
  end
end
