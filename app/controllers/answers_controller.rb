class AnswersController < ApplicationController
  before_action :set_answer, only: [:edit, :update, :destroy, :show]
  skip_before_action :authenticate_user!, only: [:show]
  respond_to :html
  
  # Shows a new answer after failed validation.
  # Used to render the new page in case of validation error so that user can fix their mistakes. 
  # WILL NOT work to go alone because requires @question variable to be set.
  def new
  end
  
  
  # Shows an answer.
  # Show method simplifies getting to a specific answer. Since Answer has no show page of its own
  # it will grab its question and link to an anchor (in the page for each answer) so that when 
  # clicked it it takes user to that part of the page.
  def show
    redirect_to question_path(@answer.question, anchor: "answer-#{@answer.id}")
  end
 
  # Creates an answer
  # Requires a user to be signed in otherwise cannot build the answer off of the current_user.
  # On successful creation a success flash with the appropriate message is displayed. Will redirect to the answers question.
  # Will also send email to let question owner know that answer has been posted.
  # On creation failure redirect to answer new to fix errors.
  def create
    @answer = current_user.answers.build(answer_params)
    if @answer.save
      flash[:success] = t('messages.models.general.post', model: "Answer")
      ahoy.track "public_created_answer", id: @answer.id
      PostableMailer.new_answer_on_post_email(@answer.question, @answer).deliver_later!
      redirect_to question_path(@answer.question)
    else
      respond_with(@answer)
    end
  end
  
  # Edit an answer.
  # Authorizes user to edit question.
  # Assigns answers @question to display while editing.
  def edit
    authorize(@answer)
    @question = @answer.question
  end
  
  # Destroys an answer.
  # Authorizes to assure can destroy.
  # Gets the answers question for after delete redirect.
  # Destroys the answer and then sets the flash to appropriate text.
  def destroy
    authorize(@answer)
    q = @answer.question
    aId = @answer.id
    @answer.destroy
    flash[:success] = t('messages.models.general.delete', model: "Answer")
    ahoy.track "public_created_answer", id: aId
    redirect_to question_path(q)
  end
  
  # Updates an answer.
  # Authorizes the user to make sure they can edit.
  # Depending on if update is for marking an answer correct or just updating an answer the flash will change.
  # Once successful update redirect to answers question. 
  # If fail update redirect to fix answer issue. 
  # Should never be an issue with updating the correctness of the answer (hopefully).
  def update
    if answer_params.has_key?("correct_answer_id") && (answer_params.length == 1)
      authorize(@answer, :can_select_correct_answer?)
      @answer.question.correct_answer = nil
      answer_flash =  t('messages.models.answer.marked_correct')
    else
      authorize(@answer)
      answer_flash = t('messages.models.general.update', model: "Answer")
    end
    if @answer.update(answer_params)
      flash[:success] = answer_flash
      ahoy.track "public_updated_answer", id: @answer.id
      redirect_to question_path(@answer.question)
    else
      respond_with(@answer)
    end
  end
  
  
private
  # Sets the answer, reduces code duplication.
  def set_answer
    @answer = Answer.find(params[:id])
  end
  
  #  Strong params, the params that are let through to be processed. Prevents malicious addition of additional params.
  def answer_params
    params.require(:answer).permit(:id, :answer_text, :question_id, :user_id, :correct_answer_id, :spread_sheet_upload_attributes => [:id, :sheet, :_destroy])
  end
end
