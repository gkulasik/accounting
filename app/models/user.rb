class User < ActiveRecord::Base
  
  # Devise
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
         
  # Non-model fields
  attr_accessor :delete_image
  
  # Validations
  validates :username, presence: true
  validates :username, :uniqueness => { :case_sensitive => false}
  validates :email, :uniqueness => { :case_sensitive => false}
  validate :real_username_choice
  validate :username_valid
  
  # Callbacks
  before_save :sanitize_user
  before_validation  :delete_image?
  
  # Relationships
  has_many :answers, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :visits, dependent: :destroy
  has_many :ahoy_events, class_name: "Ahoy::Event", dependent: :destroy
  
  # Paperclip
  # Avatar attached to User
  has_attached_file :avatar, styles: {
    thumb: '50x50#',
    small: '150x150#',
    }, path: ":username/avatar/:style/:basename.:extension", :default_url => ""
    
  # Validations
  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  # No huge images allowed
  validates_with AttachmentSizeValidator, :attributes => :avatar, :less_than => 1.megabytes 
  
  # Method to check if either the first name or last name or both are blank.
  def first_last_blank?
    self.first_name.blank? || self.last_name.blank?
  end
  
  # Method that outputs the users preffered name. 
  # Checks that pref_name is 'real' for real name and that neither the first or last name is blank.
  # Else if check fails then returns the username. 
  def output_name
    if self.pref_name == "real" && !self.first_last_blank?
      "#{self.first_name} #{self.last_name}"
    else
      self.username
    end
  end
  
  # Method that sanitizes the user summary to make sure no script tags get through.
  def sanitize_user
    self.user_summary = html_sanitizer(self.user_summary)
  end
  
  # Method that checks whether the user selected to delete their current image. 
  # If so then deletes it. 
  # Users will use this when they want to delete their image but not upload a new one. 
  def delete_image?
    if self.delete_image == "1"
      self.avatar = nil
    end
  end
  
  # Validation check
  # Checks that IF pref_name is 'real' that neither the first or last name is blank
  # If they are then an error will appear.
  def real_username_choice
    if self.pref_name == "real" && first_last_blank?
      errors.add(:pref_name, "First and Last name must be filled in")
    end
  end
  
  # Validation check
  # 
  def username_valid
    if(!self.username.nil? && self.username.include?("."))
      errors.add(:username, "Username cannot contain: '.'")
    end
  end
  
  # Method that allows for the usage of :username in the paperclip path
  Paperclip.interpolates :username do |attachment, style|
   attachment.instance.username
  end
  
  
  # These 3 methods are used to allow users to sign in using either username or email
  # Devise
  def login=(login)
    @login = login
  end
  # Devise
  def login
    @login || self.username || self.email
  end
  # Devise
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
  
  # Devise
  # This method takes advantage of active job to queue emails to be sent later when available resources exist
  def send_devise_notification(notification, *args)
  devise_mailer.send(notification, self, *args).deliver_later
  end
end
