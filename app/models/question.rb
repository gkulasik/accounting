class Question < ActiveRecord::Base
  # PgSearch include to provide methods
  # Multisearchable to be able to search multiple models at once. Searches on title, and question_text.
  # Orders to provide consistency in output (and correct pagination).
  include PgSearch
  multisearchable :against => [:title, :question_text], :order_within_rank => "questions.updated_at DESC"
  
  # Tags model call which creates a tagging relationship with Question on 'tags'
  acts_as_taggable_on :tags
  
  # Validations
  validates :title, :question_text, :user_id, presence: true
  validates :title,  length: {minimum: 15}
  validates :question_text, length: {minimum: 30}
  validates :tag_list, length: {minimum: 1, message: "must have at least one tag."}
  
  # Call backs
  before_save :sanitize_question
  
  # Scopes
  default_scope {includes(:user)}
  # Scopes used for filtering without having to add custom logic in other methods
  scope :newest, -> (act) {order('created_at DESC') if act} # Sort by newest
  scope :unanswered, -> (act) {joins('left outer join answers on answers.correct_answer_id = questions.id where answers.answer_text IS NULL').order('created_at ASC') if act } # Filter out answered questions
  scope :recently_updated, -> (act) {order('updated_at DESC') if act} # Sort by recently updated
  scope :with_tags, -> (tag) {tagged_with(tag).order('created_at ASC')} # Filter by tag
  scope :tag_cloud_popular, -> (act) {tag_counts_on(:tags).order('taggings_count DESC')} # Tag cloud by most popular tag (most used)
  scope :tag_cloud_newest, -> (act) {tag_counts_on(:tags).order('id DESC')} # Tag cloud by newest tag
  
  # Relationships
  belongs_to :user
  has_many :answers, dependent: :destroy
  has_one  :correct_answer, :class_name=> "Answer", foreign_key: "correct_answer_id"
  has_many :comments, as: :commentable, dependent: :destroy
  has_one :spread_sheet_upload, as: :attachable, dependent: :destroy
  
  # Relationship Modifiers
  accepts_nested_attributes_for :spread_sheet_upload, allow_destroy: true
  
  # Method to customize how :id param shows in URL
  # Have it display id-question_title
  def to_param
    "#{id}-#{title.parameterize}"
  end
  
  # Method to tell if a question is answered. Queries to find a correct answer if found return found if not false.
  def answered?
    self.correct_answer || false
  end
  
  def views
    Ahoy::Event.where(name: "private_view_question", item_id: self.id).count
  end
  
  # Method used to sanitize the content of question_text. It removes script tags.
  def sanitize_question
    self.question_text = html_sanitizer(self.question_text)
  end
  
  # Method that is used to return the question's spread sheet upload or a new spread sheet upload object.
  def get_spread_sheet_upload
    return self.spread_sheet_upload || self.build_spread_sheet_upload
  end

end
