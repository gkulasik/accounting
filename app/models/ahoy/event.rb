module Ahoy
  class Event < ActiveRecord::Base
    # Declaration that helps active record find the table.
    self.table_name = "ahoy_events"
    
    # Relationships
    belongs_to :visit
    belongs_to :user
  end
end
