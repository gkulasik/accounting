class Answer < ActiveRecord::Base
  # PgSearch include to provide methods
  # Multisearchable to be able to search multiple models at once. Searches on answer_text only.
  # Orders to provide consistency in output (and correct pagination).
  include PgSearch
  multisearchable :against => :answer_text, :order_within_rank => "answers.updated_at DESC"
  
  # Validiations
  validates :user_id, :question_id, :answer_text, presence: true
  
  # Callbacks
  before_save :sanitize_answer
  
  # Scopes
  default_scope {includes(:user, :question)}
  
  # Relationships
  belongs_to :question
  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  has_one :spread_sheet_upload, as: :attachable, dependent: :destroy
  
  # Relationship modifiers
  accepts_nested_attributes_for :spread_sheet_upload, allow_destroy: true # Needs to be able to edit/delete its spread_sheet_upload.
    
  # Method that returns the answer's question (if marked as correct answer) or false (if not)
  def correct?
    Question.find_by(id: self.correct_answer_id) || false
  end
  
  # Method that is used to return the answer's spread sheet upload or a new spread sheet upload object.
  def get_spread_sheet_upload
    return self.spread_sheet_upload || self.build_spread_sheet_upload
  end
  
  # Method that takes a answers text and sanitizes it to remove JS
  def sanitize_answer
    self.answer_text = html_sanitizer(self.answer_text)
  end
 
end


