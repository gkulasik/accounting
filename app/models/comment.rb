class Comment < ActiveRecord::Base
  # Validations
  validates :user_id, :content, :commentable_id, :commentable_type, presence: true
  
  # Scopes
  default_scope {includes(:user)}
  scope :default_order, ->  { order('created_at ASC') } 
  
  # Relationships
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  
  # Method that will auto link comments text. 
  auto_html_for :content do
    html_escape
    link :target => "_blank", :rel => "nofollow"
  end
  
end
