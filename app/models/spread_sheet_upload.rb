class SpreadSheetUpload < ActiveRecord::Base
    
    # Relationships
    belongs_to :attachable, polymorphic: true
    
    # Paperclip Modifiers
    # Path of the spread sheet will be in a folder labeled 'answer' or 'question' then in a folder by the answer/questions id. 
    # Ex. question/4/answer.xls (where the associated question has an id of 4)
    has_attached_file :sheet, path: ":attached_type/:attached_id/:basename.:extension", :default_url => ""
    
    # Validations
    # Validates content type, CVS, XLS, XLSX in that order in array
    validates_attachment_content_type :sheet, content_type: ['text/csv', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'], message: "Must be CSV, or Excel file (.xls, .xlsx)"
    validates_with AttachmentSizeValidator, :attributes => :sheet, :less_than => 2.megabytes, message: "Can not be larger than 2 megabyte"

    # Method that allows for the usage of :attached_type in the paperclip path
    Paperclip.interpolates :attached_type do |attachment, style|
     attachment.instance.attachable.class.to_s.downcase
    end
    
    # Method that allows for the usage of :attached_id in the paperclip path
    Paperclip.interpolates :attached_id do |attachment, style|
     attachment.instance.attachable.id
    end
  
#   commented out 8/1/2015
    # def no_empty_attachments
    #  if self.sheet_file_name.nil?
    #   errors[:base] << "Spreadsheet error"
    #  end
    # end
end
