json.array!(@questions) do |question|
  json.extract! question, :id, :title, :question_text, :duplicate, :answered
  json.url question_url(question, format: :json)
end
