class QuestionPolicy < ApplicationPolicy
  
  # Override: Applys super then checks to make sure that question has no comments or answers.
  # Admin permission must be repeated because admin can delete even with answers or comments.
  def destroy?
    if super && (((record.answers.count == 0) && (record.comments.count == 0)) || user.try(:admin))
      true
    else
      false
    end
  end
  # Override: Provides the admin with public view if they want it.
  # Basically shows the public or admin index view for Questions.
  def index?
    if user.try(:admin) && !params[:public_view]
      true
    else
      false
    end
  end

end