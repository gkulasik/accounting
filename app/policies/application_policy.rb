class ApplicationPolicy
  attr_reader :user, :record
  # Sets user and record
  # User = current_user
  # Record = record passed in
  def initialize(user, record)
    @user = user
    @record = record
  end
  
  # Default: Can always view index. For now.
  def index?
    true
  end
  # Default: Can only show if the record exists (will throw unauthorized error if id does not exist)
  def show?
    scope.where(:id => record.id).exists?
  end
  
  # Default: Can always create
  def create?
    true
  end
  # Default: Can always make new
  def new?
    create?
  end

  # Default: Can only update a record if the record's user is the current_user. Admin can always edit.
  def update?
    if (record.user == user) || (user.try(:admin))
      true
    else
      false
    end
  end
  
  # Default: Permission to edit comes from permission to update (since they are directly related). 
  def edit?
    update?
  end
  
  # Default: Permisson to delete comes from permission to update (since delete is a kind of edit).
  def destroy?
    update?
  end
  
  # Default: Checks if can post content. Users can only post when they are logged in. 
  # This permission is mostly for post content: questions, answers, and comments
  def can_post?
    if user.nil?
      false
    else
      true
    end
  end

  # Default Pundit auto generated. Helps with determining the policy to check (depending on object provided).
  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end

