class AnswerPolicy < ApplicationPolicy
  
  # Override: Cannot delete answer if has commments. Admin can always delete answer.
  def destroy?
    if super && (record.comments.empty? || user.try(:admin))
      true
    else
      false
    end
  end
  
  # Custom: Checks that the currently signed in user is the question owner. 
  # If it is the question owner than they can mark the answer as correct.
  # Admin can always select correct answer.
  def can_select_correct_answer?
    if (record.question.user == user) || user.try(:admin)
      true
    else
      false
    end
  end

  # Custom: Checks if the user is posting a second answer for the same question.
  # If true then will put message up for user to see else no message. 
  # Allows multiple posts (if answers are significantly different or something.)
  def second_post?
    if user.nil? 
      return false 
    end
    if !(record.question.answers.where(user_id: user.id) - [record]).empty? && record.id.nil?
        true
      else
        false
    end
  end 
  
  
  
end