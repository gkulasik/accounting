class UserPolicy < ApplicationPolicy
  # Override: Check that the user logged in is the same as the one being edited.
  # User is now the record so the record has no '.user'
  # Admin can always edit.
  def edit?
    if (user == record) || user.try(:admin)
      true
    else
      false
    end
  end
  
end