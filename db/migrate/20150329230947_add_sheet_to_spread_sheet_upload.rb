class AddSheetToSpreadSheetUpload < ActiveRecord::Migration
  def self.up
    add_attachment :spread_sheet_uploads, :sheet
  end

  def self.down
    remove_attachment :spread_sheet_uploads, :sheet
  end
end
