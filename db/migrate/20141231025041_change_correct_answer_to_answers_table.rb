class ChangeCorrectAnswerToAnswersTable < ActiveRecord::Migration
  def change
    remove_column :questions, :correct_answer_id, :integer
    add_column :answers, :correct_answer_id, :integer
    add_index :answers, :correct_answer_id, unique: true
  end
end
