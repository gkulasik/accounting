class RemoveForeignKeys < ActiveRecord::Migration
  def change
    # fix foreign keys
    remove_foreign_key  :comments, :users
  add_foreign_key :comments, :users, on_delete: :cascade
    remove_foreign_key :answers, :questions
  add_foreign_key :answers, :questions, on_delete: :cascade
    remove_foreign_key :answers, :users
  add_foreign_key :answers, :users, on_delete: :cascade
  end
end
