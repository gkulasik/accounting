class CreateSpreadSheetUploads < ActiveRecord::Migration
  def change
    create_table :spread_sheet_uploads do |t|
      t.references :attachable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
