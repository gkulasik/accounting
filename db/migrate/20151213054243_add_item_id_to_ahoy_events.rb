class AddItemIdToAhoyEvents < ActiveRecord::Migration
  def change
    add_column :ahoy_events, :item_id, :integer
  end
end
