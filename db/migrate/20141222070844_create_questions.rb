class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :title
      t.text :question_text
      t.text :duplicate
      t.boolean :answered, default: false

      t.timestamps null: false
    end
  end
end
