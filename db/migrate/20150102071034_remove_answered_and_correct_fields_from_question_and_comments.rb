class RemoveAnsweredAndCorrectFieldsFromQuestionAndComments < ActiveRecord::Migration
  def change
    remove_column :questions, :answered, :boolean
    remove_column :answers, :correct, :boolean
  end
end
