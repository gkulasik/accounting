class AddForeignKeyToEventsAndVisits < ActiveRecord::Migration
  def change
    add_foreign_key :ahoy_events, :visits, on_delete: :cascade
  end
end
